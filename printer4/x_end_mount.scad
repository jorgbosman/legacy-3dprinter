// X-motor mount
//
// jorg@bosman.tv - 2014-05-23

module mount() {
  difference() {
    union() {
      translate([-21,1,-17]) cube([41,20,20]);

      //west mount
      translate([-41,1,0]) cube([41,20,3]);
      translate([-41,1,-17]) cube([20,40,20]);
      translate([-41,21,-24]) cube([20,20,20]);

      //north mount
      //#translate([0,1,0]) cube([20,39,3]);
      //#translate([0,21,-17]) cube([20,19,20]);

    }

    // bolt hole
    translate([0, 10, -18]) cylinder(r=2.7, h=20+2, $fn=120);
    translate([0, 10, -18]) cylinder(r=4.8, h=9, $fn=6);

    // west mount
    translate([-21,1,-18]) rotate([0,0,-225]) cube([29,15,22]);
    translate([-42,31,-12]) rotate([0,90,0]) cylinder(r=2.8, h=22, $fn=28);
    translate([-42,31,-12]) rotate([0,90,0]) cylinder(r=5.5, h=17, $fn=55);

    //north mount
    translate([0,0,-18]) rotate([0,0,-45]) cube([15,30,22]);
    translate([10,22,-7]) rotate([90,0,0]) cylinder(r=2.8, h=16, $fn=28);
    translate([10,17,-7]) rotate([90,0,0]) cylinder(r=5.5, h=14, $fn=55);

    // belt guide
    translate([-31,31,-7]) cylinder(r=2.65, h=11, $fn=25);
    translate([-31,31,-10]) cylinder(r=4.8, h=6, $fn=6);
  }
}

translate([0,0,0]) rotate([0,180,0]) mirror([0,1,0]) mount();
translate([0,0,0]) rotate([0,180,0]) mirror([0,0,0]) mount();
