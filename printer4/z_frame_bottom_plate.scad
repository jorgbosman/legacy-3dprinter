// z-frame bottom plate
//
// jorg@bosman.tv - 2014-07-06

module plate() {
  difference() {
    union() {
      translate([-30, -10, 0]) cube([60, 20, 3]);
      translate([-10, -10, 0]) cube([20, 80, 3]);
    }
    translate([  0,0,-1]) cylinder(r=2.7, h=5, $fn=27);
    translate([-20,0,-1]) cylinder(r=2.7, h=5, $fn=27);
    translate([ 20,0,-1]) cylinder(r=2.7, h=5, $fn=27);
    translate([  0,20,-1]) cylinder(r=2.7, h=5, $fn=27);
    translate([  0,40,-1]) cylinder(r=2.7, h=5, $fn=27);

    // cable guide
    translate([-5, 55, -1]) cube([10, 10, 5]);
    translate([-5, 55, -1]) cube([16, 5, 5]);

  }
}

plate();
