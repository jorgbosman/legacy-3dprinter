// X-motor mounts
//
// jorg@bosman.tv - 2014-05-23

module motor_mount_top() {
  difference() {
    union() {
      translate([-21-0.2,2,0]) cube([42+0.4,19,3]);

      //west mount
      //#translate([-41,2,0]) cube([41,19,3]);
      translate([-40,2,-17]) cube([18.9,39,20]);
      translate([-40,21,-22]) cube([18.9,20,20]);

      //north mount
      //#translate([0,2,0]) cube([20,38,3]);
      translate([21.1,2,-17]) cube([19,19,20]);

    }

    // motor holes
    translate([0, 0, -1]) cylinder(r=12, h=6+2, $fn=120);
    hull() {
      translate([-15.5-0.5,  15+0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
      translate([-15.5+0.5,  15-0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
    }
    hull() {
      translate([ 15.5-0.5,  15-0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
      translate([ 15.5+0.5,  15+0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
    }

    // west mount
    translate([-21,1,-18]) rotate([0,0,-225]) cube([29,15,22]);
    translate([-41,31,-12]) rotate([0,90,0]) cylinder(r=2.8, h=22, $fn=28);
    translate([-41,31,-12]) rotate([0,90,0]) cylinder(r=5.5, h=17, $fn=55);

    //north mount
    translate([22,2,-18]) rotate([0,0,-45]) cube([15,26,22]);
    translate([30,22,-7]) rotate([90,0,0]) cylinder(r=2.8, h=16, $fn=28);
    translate([30,17,-7]) rotate([90,0,0]) cylinder(r=5.5, h=13, $fn=55);

    // belt guide
    translate([-31,31,-7]) cylinder(r=2.65, h=11, $fn=25);
    translate([-31,31,-10]) cylinder(r=4.8, h=6, $fn=6);
  }
}

translate([0,0,0]) rotate([0,180,0]) motor_mount_top();
translate([0,0,0]) rotate([0,0,180]) mirror([0,0,1]) motor_mount_top();
//#translate([-21,0,0]) cube([42,42,42]);