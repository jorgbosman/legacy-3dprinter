// end stop mount
//
// jorg@bosman.tv - 2014-07-01

module end_stop() {
  difference() {
    union() {
      translate([-12.2,-12,-8]) cube([2,22,16]);
      translate([-12.2,-12,-8]) cube([24.4,2,16]);
      translate([ 10.2,-12,-8]) cube([3,22,28]);

      translate([-10,-15,0]) rotate([-90,0,0]) cylinder(r=1.4, h=5, $fn=18);
      translate([-12,-15,-8]) cube([4,0.5,7.5]);
    }
    translate([-14,0,0]) rotate([0,90,0]) cylinder(r=2.7, h=28, $fn=27);
    translate([0,-14,0]) rotate([-90,0,0]) cylinder(r=1.8, h=5, $fn=18);

    translate([ 9.2,-8,8]) cube([5,14,8]);
  }
}

end_stop();

