// Belt guide
//
// jorg@bosman.tv - 2014-05-23

module guide() {
  difference() {
    union() {
      cylinder(r=8, h=1.7, $fn=80);
      cylinder(r=6, h=5, $fn=60);
    }
    translate([0,0,-1]) cylinder(r=4.8, h=7, $fn=50);
    translate([0,0,1]) cylinder(r=5.16, h=7, $fn=50);
  }
}

guide();
