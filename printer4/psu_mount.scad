// psu mount to frame
//
// jorg@bosman.tv - 2014-07-03

module psu_mount() {
  difference() {
    union() {
      translate([-50,-20,0]) cube([100,105,3]);
      translate([-50,0,0]) cube([100,3,43]);
      translate([-50,0,0]) cube([100,7,6]);
      translate([-50,0,0]) cube([100,13,4]);

      // left side
      translate([-50,0,13]) cube([9,23,30]);
      translate([-50,22.5,0]) cube([9,0.5,14]);

      // right side
      translate([47,0,0]) cube([3,13,43]);
    }

    // frame holes
    translate([-40,-10,-1]) cylinder(r=2.7,h=5, $fn=27);
    translate([ 40,-10,-1]) cylinder(r=2.7,h=5, $fn=27);

    // psu holes
    translate([-42,18,-1]) cylinder(r=1.7,h=5, $fn=17);
    translate([ 45,19,-1]) cylinder(r=1.7,h=5, $fn=17);
    translate([-42+4,18+58,-1]) cylinder(r=1.7,h=5, $fn=17);
    translate([ 45-4,19+57,-1]) cylinder(r=1.7,h=5, $fn=17);
  }
}

psu_mount();
