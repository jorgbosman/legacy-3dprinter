// X-motor mounts
//
// jorg@bosman.tv - 2014-05-23

module motor_mount_bottom() {
  difference() {
    union() {
      translate([-21-0.2,2,0]) cube([21+0.2,19,3]);

      //west mount
      translate([-40,2,-17]) cube([18.9,39,20]);
    }

    // motor holes
    translate([0, 0, -1]) cylinder(r=12, h=6+2, $fn=120);
    hull() {
      translate([-15.5-0.5,  15+0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
      translate([-15.5+0.5,  15-0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
    }
    hull() {
      translate([ 15.5-0.5,  15-0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
      translate([ 15.5+0.5,  15+0.5, -1]) cylinder(r=1.8, h=3+2, $fn=18);
    }

    // west mount
    translate([-21,1,-18]) rotate([0,0,-225]) cube([29,15,22]);
    translate([-41,31,-7]) rotate([0,90,0]) cylinder(r=2.8, h=22, $fn=28);
    translate([-41,31,-7]) rotate([0,90,0]) cylinder(r=5.5, h=17, $fn=55);
  }
}

translate([0,0,0]) rotate([0,180,0]) motor_mount_bottom();
translate([0,0,0]) rotate([0,0,180]) mirror([0,0,1]) motor_mount_bottom();
