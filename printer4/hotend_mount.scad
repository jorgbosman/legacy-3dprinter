// Extruder for 2 e3d-online v5 hotends
//
// jorg@bosman.tv - 2013-11-05


module hotend() {
  difference() {
    union() {
      color("silver") {
        translate([0,0,-51]) cylinder(r1=0.2, r2=2, h=2, $fn=20);
        translate([0,0,-49]) cylinder(r=4, h=3, $fn=6);
        translate([-8,-12,-46]) cube([16,16,12]);
        translate([0,0,-32]) cylinder(r=12.5, h=32, $fn=125);
        translate([0,0,-1]) cylinder(r=4.8, h=10, $fn=40);
        translate([0,0, 1.8]) cylinder(r=8.8, h=2.1, $fn=80);
        translate([0,0, 5.5]) cylinder(r=8.3, h=3.6, $fn=80);
        translate([0,0, 7]) cylinder(r=6.6, h=8, $fn=60);
        translate([0,0,13.8]) cylinder(r=8.8, h=4.2+20, $fn=80);
      }
      //color("blue") translate([-16,-8.5,-31]) cube([32, 27, 30]);
      //color("black") translate([-15,-8.5+27,-31]) cube([30, 12, 30]);
    }

    //translate([0,0,-33]) cylinder(r=1.5, h=33+13.8+5.2, $fn=15);
  }
}

module fan() {
  difference() {
    color("gray") translate([-5,-15,-15]) cube([10,30,30]);
    translate([-6,0,0]) rotate([0,90,0]) cylinder(r=14,h=12);
    translate([-6,-15+3, -15+3]) rotate([0,90,0]) cylinder(r=1.8, h=12, $fn=18);
    translate([-6, 15-3, -15+3]) rotate([0,90,0]) cylinder(r=1.8, h=12, $fn=18);
    translate([-6,-15+3,  15-3]) rotate([0,90,0]) cylinder(r=1.8, h=12, $fn=18);
    translate([-6, 15-3,  15-3]) rotate([0,90,0]) cylinder(r=1.8, h=12, $fn=18);
  }
}

module hotend_mount_part1() {
  difference() {
    union() {
      translate([-12,-26,-12]) cube([30.5,52,15.5]);
      translate([-12,-26,-12]) cube([24,52,24]);
      translate([-12,-26,-12]) cube([12,52,36]);
      translate([0,-26,12]) rotate([-90,0,0]) cylinder(r=12,h=52,$fn=120);
      translate([-12,0,0]) rotate([0,90,0]) cylinder(r=12,h=30.5,$fn=120);
    
      translate([-12, -28.5, -4.5]) cube([30.5, 4, 8]);
      translate([-12,  24.5, -4.5]) cube([30.5, 4, 8]);
      translate([-12, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=30.5,$fn=40);
      translate([-12,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=30.5,$fn=40);

      // ventilator mount
      translate([-12,-19,-37.5]) cube([4,38,26.5]);
      translate([-12,-7,-42.2]) cube([4,14,10]);
      translate([-12,-6.5,-39.5]) rotate([0,90,0]) cylinder(r=2.7, h=4, $fn=25);
      translate([-12, 6.5,-39.5]) rotate([0,90,0]) cylinder(r=2.7, h=4, $fn=25);
    }
    // y-axis bushings
    translate([0,-27,12]) rotate([-90,0,0]) cylinder(r=8.2,h=54,$fn=80);
    translate([0,-31,12]) rotate([-90,0,0]) cylinder(r=9,h=5,$fn=80);
    translate([0, 26,12]) rotate([-90,0,0]) cylinder(r=9,h=5,$fn=80);

    // x-axis bushings
    translate([-13,0,0]) rotate([0,90,0]) cylinder(r=8.2,h=32.5,$fn=80);

    translate([19,-19,-12]) hotend();
    translate([19, 19,-12]) hotend();

    translate([-13, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=32.5,$fn=18);
    translate([-13,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=32.5,$fn=18);

    // ventilator holes
    translate([-13,-19,-27]) rotate([0,90,0]) cylinder(r=15, h=6, $fn=145);
    translate([-13, 19,-27]) rotate([0,90,0]) cylinder(r=15, h=6, $fn=145);
    translate([-13,-6.5,-14.5]) rotate([0,90,0]) cylinder(r=1.7, h=6, $fn=16);
    translate([-13, 6.5,-14.5]) rotate([0,90,0]) cylinder(r=1.7, h=6, $fn=16);
    translate([-13,-6.5,-39.5]) rotate([0,90,0]) cylinder(r=1.7, h=6, $fn=16);
    translate([-13, 6.5,-39.5]) rotate([0,90,0]) cylinder(r=1.7, h=6, $fn=16);
  }
}

module hotend_mount_part2() {
  difference() {
    union() {
      translate([19,-26,-12]) cube([10,52,15.5]);
      translate([19,  0, 0]) rotate([0,90,0]) cylinder(r=12,h=10,$fn=120);
      translate([19, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=10,$fn=40);
      translate([19,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=10,$fn=40);
      translate([19, -28.5, -4.5]) cube([10, 4, 8]);
      translate([19,  24.5, -4.5]) cube([10, 4, 8]);


    }
    // x-axis busing
    translate([18,  0, 0]) rotate([0,90,0]) cylinder(r=8.2,h=12,$fn=120);

    translate([19,-19,-12]) hotend();
    translate([19, 19,-12]) hotend();

    translate([18, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=12,$fn=18);
    translate([18,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=12,$fn=18);
  }
}

module fan_to_fan() {
  difference() {
    union() {
      translate([2, -37, -45]) cube([4, 74, 9]);
      translate([2, -41, -45]) cube([33, 21, 9]);
      translate([2,  39-19, -45]) cube([33, 21, 9]);

      // fan pin
      translate([0,-6.5-24,-39]) rotate([0,90,0]) cylinder(r=1.7,h=5, $fn=18);
      translate([0, 6.5+24,-39]) rotate([0,90,0]) cylinder(r=1.7,h=5, $fn=18);

      // fan pin support
      translate([0,-6.5-25.5,-45]) cube([0.2, 3, 5]);
      translate([0, 6.5+25.5-3,-45]) cube([0.2, 3, 5]);
    }
    translate([1,-19,-27]) rotate([0,90,0]) cylinder(r=14.5,h=35, $fn=150);
    translate([1, 19,-27]) rotate([0,90,0]) cylinder(r=14.5,h=35, $fn=150);

    // hotend cutout
    translate([19,-19,-46]) cylinder(r=13, h=11, $fn=100);
    translate([19, 19,-46]) cylinder(r=13, h=11, $fn=100);

    // diagonal fan
    translate([1, -42, -46]) rotate([45,0,0]) cube([35, 15, 7]);
    translate([19, -45, -52]) rotate([15,0,0]) cylinder(r=14.5,h=12);
    translate([7, -26, -47.3]) rotate([45,0,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([7+24, -26, -47.3]) rotate([45,0,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([7, -30.5, -42.8]) rotate([45,0,0]) cylinder(r=3.4, h=3, $fn=6);
    translate([7+24, -30.5, -42.8]) rotate([45,0,0]) cylinder(r=3.4, h=3, $fn=6);
    translate([3.6, -35.5, -47.8]) rotate([45,0,0]) cube([6.3,7.2,3]);
    translate([3.6+24, -35.5, -47.8]) rotate([45,0,0]) cube([6.3,7.2,3]);
    translate([6.5+20, -25.5, -45.3]) cube([9,9,7]);

    mirror([0,1,0]) {
      translate([1, -42, -46]) rotate([45,0,0]) cube([35, 15, 7]);
      translate([19, -45, -52]) rotate([15,0,0]) cylinder(r=14.5,h=12);
      translate([7, -26, -47.3]) rotate([45,0,0]) cylinder(r=1.8, h=13, $fn=18);
      translate([7+24, -26, -47.3]) rotate([45,0,0]) cylinder(r=1.8, h=13, $fn=18);
      translate([7, -30.5, -42.8]) rotate([45,0,0]) cylinder(r=3.4, h=3, $fn=6);
      translate([7+24, -30.5, -42.8]) rotate([45,0,0]) cylinder(r=3.4, h=3, $fn=6);
      translate([3.6, -35.5, -47.8]) rotate([45,0,0]) cube([6.3,7.2,3]);
      translate([3.6+24, -35.5, -47.8]) rotate([45,0,0]) cube([6.3,7.2,3]);
      translate([6.5+20, -25.5, -45.3]) cube([9,9,7]);
    }

    // m3 holes
    translate([1,-7,-39]) rotate([0,90,0]) cylinder(r=1.8,h=6, $fn=18);
    translate([1, 7,-39]) rotate([0,90,0]) cylinder(r=1.8,h=6, $fn=18);
    
  }
}

module hotend_mount() {
    hotend_mount_part1();
    translate([0,0,0]) hotend_mount_part2();

    translate([19,-19,-12]) mirror([0,1,0]) hotend();
    translate([19, 19,-12]) hotend();

    fan_to_fan();

    //fan
    translate([-3,18.5,-27]) fan();
    translate([-3,-18.5,-27]) fan();

    translate([19,47,-44]) rotate([0,-45,90]) fan();
    translate([19,-47,-44]) rotate([0,45,90]) fan();
}

module print_bed() {
    translate([-10,0,0]) rotate([180,-90,0]) hotend_mount_part1();
    translate([30,0,17]) rotate([0, 90,0]) hotend_mount_part2();
    translate([-65,0,31]) fan_to_fan();
}

//hotend_mount();
print_bed();
//test_hotend_fit();
