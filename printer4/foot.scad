// Feet
//
// jorg@bosman.tv - 2014-05-28

module foot() {
  difference() {
    union() {
      hull() {
        translate([-8,-8,0]) cylinder(r=2, h=5, $fn=20);
        translate([ 8,-8,0]) cylinder(r=2, h=5, $fn=20);
        translate([-8, 8,0]) cylinder(r=2, h=5, $fn=20);
        translate([ 8, 8,0]) cylinder(r=2, h=5, $fn=20);
      }
    }
    translate([0,0,-1]) cylinder(r=2.65, h=7, $fn=26);
    translate([0,0, 3]) cylinder(r=5, h=7, $fn=26);
  }
}

foot();
