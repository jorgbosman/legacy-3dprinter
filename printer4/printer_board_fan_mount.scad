// Fan mount for printer board
//
// jorg@bosman.tv - 2014-07-29

module fan_mount() {
  difference() {
    union() {
      translate([-20,-10,0]) cube([40,20,20]);
      translate([-20,-10,0]) cube([40,30,3]);
      translate([-12,-10,0]) cube([24,60,3]);
    }
    // mount holes
    translate([-10, 0, -1]) cylinder(r=2.8, h=22, $fn=27);
    translate([ 10, 0, -1]) cylinder(r=2.8, h=22, $fn=27);

    // fan holes
    translate([-25, 30, -1]) cylinder(r=20, h=5, $fn=190);
    translate([ 25, 30, -1]) cylinder(r=20, h=5, $fn=190);

    // fan mount holes
    translate([-9, 14, -1]) cylinder(r=2.2, h=5, $fn=190);
    translate([ 9, 14, -1]) cylinder(r=2.2, h=5, $fn=190);
    translate([-9, 46, -1]) cylinder(r=2.2, h=5, $fn=190);
    translate([ 9, 46, -1]) cylinder(r=2.2, h=5, $fn=190);

  }
}

fan_mount();
