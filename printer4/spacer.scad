// spacers for the belt-guides
//
// jorg@bosman.tv - 2014-07-06

difference() {
  union() {
    cylinder(r=4, h=6.35, $fn=30);
  }
  translate([0,0,-1]) cylinder(r=2.7, h=8, $fn=17);
}
