// mount plate for voltage regulator
//
// jorg@bosman.tv - 2014-07-05

module plate() {
  difference() {
    union() {
      translate([-20, -20, 0]) cube([40, 40, 3]);

      translate([-13, 8, 0]) cylinder(r=3, h=5, $fn=17);
    }
    translate([-10, -10, -1]) cylinder(r=2.7, h=5, $fn=27);
    translate([ 10, -10, -1]) cylinder(r=2.7, h=5, $fn=27);

    translate([-13, 8, -1]) cylinder(r=1.7, h=7, $fn=17);
  }
}

plate();
