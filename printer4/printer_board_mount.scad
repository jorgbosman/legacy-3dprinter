// mount plate for Megatronics v3 board
//
// jorg@bosman.tv - 2014-07-02

module plate() {
  difference() {
    union() {
      translate([-55, -10, 0]) cube([110, 30, 3]);
    }
    translate([-45, 0, -1]) cylinder(r=2.7, h=5, $fn=27);
    translate([ 45, 0, -1]) cylinder(r=2.7, h=5, $fn=27);

    translate([-55+2.5, 12.6, -1]) cylinder(r=1.7, h=5, $fn=17);
    translate([ 55-2.5, 12.6, -1]) cylinder(r=1.7, h=5, $fn=17);
  }
}

plate();
