// mount plate for raspberry pi
//
// jorg@bosman.tv - 2014-07-05

module plate() {
  difference() {
    union() {
      translate([-43, -20, 0]) cube([86, 80, 3]);
    }
    translate([-33, -10, -1]) cylinder(r=2.7, h=5, $fn=27);
    translate([ 33, -10, -1]) cylinder(r=2.7, h=5, $fn=27);

    translate([ 43-55, 60-12.5-25, -1]) cylinder(r=1.7, h=5, $fn=17);
    translate([ 43-5, 60-12.5, -1]) cylinder(r=1.7, h=5, $fn=17);

    translate([-44, 17, -1]) cube([21, 32, 5]);
  }
}

plate();
