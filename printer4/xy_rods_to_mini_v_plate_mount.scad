// mount for the x and y rods onto the mini v plate
//
// jorg@bosman.tv - 2014-05-11

module mini_v_plate() {
  difference() {
    union() {
      translate([-25,-25,0]) cube([50,50,6]);
    }
    translate([0,0,-1]) cylinder(r=2.5, h=8, $fn=25);
    translate([-10,0,-1]) cylinder(r=2.5, h=8, $fn=25);
    translate([0,-10,-1]) cylinder(r=2.5, h=8, $fn=25);
    translate([10,0,-1]) cylinder(r=2.5, h=8, $fn=25);
    translate([0,10,-1]) cylinder(r=2.5, h=8, $fn=25);
  }
}

module mount() {
  difference() {
    hull() {
      //translate([0,-4,0]) cylinder(r=6.2, h=6, $fn=25);

      translate([-10,0,0]) cylinder(r=5, h=6, $fn=50);
      translate([0,-10,0]) cylinder(r=5, h=6, $fn=50);
      translate([10,0,0]) cylinder(r=5, h=6, $fn=50);
      translate([0,10,0]) cylinder(r=5, h=6, $fn=50);
    }
    // middle hole for the 8mm rod
    translate([0,-4,-1]) cylinder(r=4.4, h=8, $fn=25);

    // holes for screws
    translate([-10,0,-1]) cylinder(r=2.7, h=8, $fn=25);
    translate([10,0,-1]) cylinder(r=2.7, h=8, $fn=25);
    translate([0,10,-1]) cylinder(r=2.7, h=8, $fn=25);

    // hole for m3 nut
    translate([-3.2,-11.6,-1]) cube([6.4,2.6,8]);
    translate([-4,-15,-1]) cube([8,2,8]);

    // hole for m3 bolt
    translate([0,-15,3]) rotate([-90,0,0]) cylinder(r=1.8, h=8, $fn=16);
  }
}


//translate([0,0,-6.1]) color("gray") mini_v_plate();
mount();
