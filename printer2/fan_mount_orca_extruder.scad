// Fan mount for the orca extruder
//
// jorg@bosman.tv - 2011-11-06

module solids() {
  // base
  cube([41,18,6]);

  // fan mounts
  cube([9,4,8]);
  translate([32,0,0]) cube([9,4,8]);
  translate([4.5,0,8]) rotate([-90,0,0]) cylinder(r=4.5, h=4);
  translate([36.5,0,8]) rotate([-90,0,0]) cylinder(r=4.5, h=4);
}

module cutouts() {
  // center holes
  translate([15.5,12,-1]) cylinder(r=2, h=8);
  translate([25.5,12,-1]) cylinder(r=2, h=8);

  // fan mount holes
  translate([4.5,-1,8]) rotate([-90,0,0]) cylinder(r=2.5, h=6);
  translate([36.5,-1,8]) rotate([-90,0,0]) cylinder(r=2.5, h=6);

  // fan mount screw holes
  translate([4.5,4,8]) rotate([-90,0,0]) cylinder(r=4.5, h=15);
  translate([36.5,4,8]) rotate([-90,0,0]) cylinder(r=4.5, h=15);
}

rotate([0,90,0]) difference() {
  solids();
  cutouts();
}
