// Modified version of gregswade extruder, to fit on the orca hotend
//
// jorg@bosman.tv - 2011-12-15

module mendelparts_solids() {
  import("../stl/gregswadev3blank_fixed.stl");
  # translate([-23,0,9]) cube([10,7,10]);
  # translate([-23+50,0,9]) cube([10,7,10]);
}

module mendelparts_v9_cutouts() {
  #translate([10,5,14]) cylinder(r=8, h=20);
  #translate([10,10,14]) cylinder(r=8, h=20);
  #translate([-16.7+24,0,14]) rotate([-90,0,0]) cylinder(r=6, h=18);

  #translate([-16.7,-1,14-5]) rotate([-90,0,0]) cylinder(r=2.5,h=9);
  #translate([-16.7,-1,14+5]) rotate([-90,0,0]) cylinder(r=2.5,h=9);

  #translate([-16.7+48,-1,14-6.25]) rotate([-90,0,0]) cylinder(r=2.5,h=9);
  #translate([-16.7+48,-1,14+6.25]) rotate([-90,0,0]) cylinder(r=2.5,h=9);
}

module makergear_solids() {
  import("../stl/gregswadev3blank_fixed.stl");
}

module makergear_cutouts() {
    # translate([7.3,-1,14]) rotate([-90,0,0]) cylinder(r=8.4, h=13);
    # translate([7.3+7.45,5.4,-1]) cylinder(r=2, h=30);
    # translate([7.3-7.45,5.4,-1]) cylinder(r=2, h=30);
}

difference() {
  makergear_solids();
  makergear_cutouts();
}
