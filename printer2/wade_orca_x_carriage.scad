// x-carriage for use with wade extruder and orca hotend
//
// jorg@bosman.tv - 2011-11-06

module lm8uu_cutouts() {
  // hole for the 8mm rod
  translate([11,-1,11]) rotate([-90,0,0]) cylinder(r=5.2,h=33);
  translate([5.9,-1,11]) cube([10.3,33,6]);

  // space for the LM8UU linear bearing
  translate([11,3,11]) rotate([-90,0,0]) cylinder(r=8,h=25);

  // openings on the sides
  translate([-1,6,4]) cube([24,6,12]);
  translate([-1,19,4]) cube([24,6,12]);

  // hole for 3mm screw
  // translate([11,15.5,-1]) cylinder(r=2,h=2);
  // translate([11,15.5,1]) cylinder(r1=2,r2=3.5,h=2);
  // translate([11,15.5,3]) cylinder(r=3.5,h=2);

  // ty-rap holes
  translate([10,7.25,-3]) rotate([0,-45,0]) cube([2, 3.5, 18]);
  translate([12,7.25,-3]) mirror([1,0,0]) rotate([0,-45,0]) cube([2, 3.5, 18]);
  translate([10,20.25,-3]) rotate([0,-45,0]) cube([2, 3.5, 18]);
  translate([12,20.25,-3]) mirror([1,0,0]) rotate([0,-45,0]) cube([2, 3.5, 18]);
}

module solids() {
  // plate
  translate([-12,0,0]) cube([83,64,4]);
  translate([71,0,0]) cube([6,58,4]);
  translate([-16,27,0]) cube([10,10,4]);

  // mendel-parts v9 plate holes
  translate([-10,27,0]) cylinder(r=6, h=4);
  translate([-10,37,0]) cylinder(r=6, h=4);
  translate([71,0,0]) cylinder(r=6, h=4);
  translate([71,58,0]) cylinder(r=6, h=4);

  translate([-10,27,0]) cylinder(r=4.5, h=5);
  translate([-10,37,0]) cylinder(r=4.5, h=5);
  translate([71,0,0]) cylinder(r=4.5, h=5);
  translate([71,58,0]) cylinder(r=4.5, h=5);


  // bearing holders
  translate([0,3.5,0]) cube([22,57,15]);
  translate([50,16.5,0]) cube([22,31,15]);

  // belt clamp
  translate([-10,-8,0]) cube([10,7,5.5]);
  translate([-12,-8,0]) cube([14,7,5.5]);
  translate([-12,-4.5,0]) cube([17,4.5,4]);
  translate([1.5,-4.5,0]) cylinder(r=3.5, h=5.5);
  translate([-11.5,-4.5,0]) cylinder(r=3.5, h=5.5);
  translate([-5,-8,2.75]) rotate([-90,0,0]) cylinder(r=2.75, h=8);

  translate([-10,65,0]) cube([10,7,5.5]);
  translate([-12,65,0]) cube([14,7,5.5]);
  translate([-12,64,0]) cube([17,4.5,4]);
  translate([1.5,68.5,0]) cylinder(r=3.5, h=5.5);
  translate([-11.5,68.5,0]) cylinder(r=3.5, h=5.5);
  translate([-5,64,2.75]) rotate([-90,0,0]) cylinder(r=2.75, h=8);

  // extra belt clamps
  translate([-10,-17,0]) cube([10,7,5.5]);
  translate([-12,-17,0]) cube([14,7,5.5]);
  translate([1.5,-13.5,0]) cylinder(r=3.5, h=5.5);
  translate([-11.5,-13.5,0]) cylinder(r=3.5, h=5.5);

  translate([-10+22,-17,0]) cube([10,7,5.5]);
  translate([-12+22,-17,0]) cube([14,7,5.5]);
  translate([1.5+22,-13.5,0]) cylinder(r=3.5, h=5.5);
  translate([-11.5+22,-13.5,0]) cylinder(r=3.5, h=5.5);

  translate([-10+44,-17,0]) cube([10,7,5.5]);
  translate([-12+44,-17,0]) cube([14,7,5.5]);
  translate([1.5+44,-13.5,0]) cylinder(r=3.5, h=5.5);
  translate([-11.5+44,-13.5,0]) cylinder(r=3.5, h=5.5);

  // fan mount
  translate([15.5,0,0]) rotate([45,0,0]) cube([41,4,7.5]);
  translate([16,0,0]) rotate([-45,0,0]) translate([4,-7,0]) cylinder(r=4.5,h=4);
  translate([16,0,0]) rotate([-45,0,0]) translate([36,-7,0]) cylinder(r=4.5,h=4);

  translate([15.5,64,0]) rotate([-45,0,0]) mirror([0,1,0]) cube([41,4,7.5]);
  translate([16,64,0]) rotate([45,0,0]) mirror([0,1,0]) translate([4,-7,0]) cylinder(r=4.5,h=4);
  translate([16,64,0]) rotate([45,0,0]) mirror([0,1,0]) translate([36,-7,0]) cylinder(r=4.5,h=4);
}

module cutouts() {
  // hotend
  translate([36,32,-1]) cylinder(r=13, h=6);

  // mounting holes
  translate([-10,27,-1]) cylinder(r=2.5, h=6);
  translate([-10,37,-1]) cylinder(r=2.5, h=6);
  translate([71,0,-1]) cylinder(r=2.5, h=6);
  translate([71,58,-1]) cylinder(r=2.5, h=6);

  // bearing holders
  translate([0,3,0]) lm8uu_cutouts();
  translate([0,30,0]) lm8uu_cutouts();
  translate([50,16.5,0]) lm8uu_cutouts();

  // belt clamp
  translate([-5,-9,2.75]) rotate([-90,0,0]) cylinder(r=2, h=10);
  translate([1.5,-4.5,-1]) cylinder(r=2, h=7.5);
  translate([-11.5,-4.5,-1]) cylinder(r=2, h=7.5);

  translate([-5,63,2.75]) rotate([-90,0,0]) cylinder(r=2, h=10);
  translate([1.5,68.5,-1]) cylinder(r=2, h=7.5);
  translate([-11.5,68.5,-1]) cylinder(r=2, h=7.5);

  // extra belt clamp
  translate([1.5,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-11.5,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-8,-18,3.5]) cube([6,9,3]);

  translate([1.5+22,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-11.5+22,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-8+22,-17.5-1,4]) cube([6,2.25,2.5]);
  translate([-8+22,-17.5+3,4]) cube([6,2.25,2.5]);
  translate([-8+22,-17.5+7,4]) cube([6,2.25,2.5]);

  translate([1.5+44,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-11.5+44,-13.5,-1]) cylinder(r=2, h=7.5);
  translate([-8+44,-17.5-1,4]) cube([6,2.25,2.5]);
  translate([-8+44,-17.5+3,4]) cube([6,2.25,2.5]);
  translate([-8+44,-17.5+7,4]) cube([6,2.25,2.5]);

  // openings for belt
  translate([-8.5,0,-1]) cube([7,10,6]);
  translate([-5,10,-1]) cylinder(r=3.5, h=6);

  translate([-8.5,54,-1]) cube([7,10,6]);
  translate([-5,54,-1]) cylinder(r=3.5, h=6);

  // fan mount
  translate([16,0,0]) rotate([-45,0,0]) translate([4,-7,-1]) cylinder(r=2.5,h=6);
  translate([16,0,0]) rotate([-45,0,0]) translate([36,-7,-1]) cylinder(r=2.5,h=6);
  translate([16,0,0]) rotate([-45,0,0]) translate([20,-23,-1]) cylinder(r=19,h=6);

  translate([16,64,0]) rotate([45,0,0]) mirror([0,1,0]) translate([4,-7,-1]) cylinder(r=2.5,h=6);
  translate([16,64,0]) rotate([45,0,0]) mirror([0,1,0]) translate([36,-7,-1]) cylinder(r=2.5,h=6);
  translate([16,64,0]) rotate([45,0,0]) mirror([0,1,0]) translate([20,-23,-1]) cylinder(r=19,h=6);

}

difference() {
  solids();
  # cutouts();
}

