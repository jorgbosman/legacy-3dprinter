// Cover for the ball-bearing, so i won't need washers
//
// jorg@bosman.tv - 2011-12-27

compensation=0.5; // keeping in mind the plastic will be a bit thicker then expected,
                  // so adding a bit more space to the bearing will still fit.
compensation1st=0.25; // 1st layer expands a bit more than the rest.

module solids() {
  cylinder(r=11.7+compensation, h=6.85); // main object
  cylinder(r1=13.7+compensation-compensation1st, r2=12.7+compensation, h=0.9); // lower edge
  translate([0,0,6.85-0.9]) cylinder(r1=12.7+compensation, r2=13.7+compensation, h=0.9); // upper edge
}

module cutouts() {
  translate([0,0,-1]) cylinder(r=11+compensation, h=6.85+2); // 608-ZZ ball bearing
}


difference() {
  solids();
  # cutouts();
}
