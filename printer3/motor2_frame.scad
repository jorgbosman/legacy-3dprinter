// Prototype for Printer #5
//
// jorg@bosman.tv - 2013-06-23


// Printer dimensions

module motor2_frame() {
  difference() {
    union() {
      // outside plates
      translate([-2, 32, -2]) cube([2+15+2, 67, 2]);
      translate([-2, 32, -2]) cube([2, 67, 2+15]);
      translate([-2, 15+42+10, -2]) cube([2+15+42-4, 2+15, 2]);
      translate([15, 33, -2]) cube([2, 42-8, 2+15]);
      translate([15, 15+42+10-2, -2]) cube([42-8, 2, 2+15]);
      translate([15, 15+42+10+15, -2]) cube([42-4, 2, 2+15]);
      translate([15, 15+42+10+15, -2]) cube([2, 15+2, 2+15]);
      // motor plate
      translate([15, 15+10, -2]) cube([42, 42, 2]);
      // prettify
      translate([15, 15+10+42-4, 0]) cube([4, 4, 15]);
    }
    // motor holes
    translate([15+21, 15+10+21, -3]) cylinder(r=12, h=4);
    translate([15+5.5, 15+10+5.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+5.5,15+10+36.5,-3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+36.5,15+10+5.5,-3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+36.5,15+10+36.5,-3]) cylinder(r=1.8, h=4, $fn=18);

    //screwdriver space
    translate([15+5.5, 15+10+36.5, 0]) cylinder(r=4, h=16, $fn=40);

    // nut holes
    translate([7.5, 32+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5-15, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5+15+2, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+42-7.5-5, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15+2, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5-15, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5, 7.5]) rotate([0,90,0]) cylinder(r=3, h=4, $fn=18);
    translate([-3, 32+7.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5+15+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+10+42+7.5+15+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+7.5+2, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+42-7.5-5, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+21, 15+10+42-3, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+10+21, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

    // prettify
    translate([15+42-4, 15+10, -3]) rotate([0,0,-45]) cube([4,6,4]);
    translate([15+42-4, 15+10+42, -3]) rotate([0,0,-45]) cube([6,4,4]);
    translate([15, 15+10+4, -3]) rotate([0,0,-45-90]) cube([4,6,4]);
  }
}

motor2_frame();
