// heatbed z-mount without the belt
//
// jorg@bosman.tv - 2013-12-30

module z_mount() {
    difference() {
        union() {
            translate([0,  25.5, -16]) cylinder(r=12, h=15, $fn=120);
            translate([0,  13.5, -16]) cube([12, 24, 15]);

            translate([10, 13.5-15+2, -16]) cube([2, 37.5+7.5-(13.5-15+2), 15]);
            translate([10,  37.5+7.5, -8.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
        }
        translate([0,  25.5, -17]) cylinder(r=8.2, h=24, $fn=40);

        // holes for the bolts
        translate([9,  13.5-7.5+1, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([9,  37.5+7.5, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    }
}

z_mount();

