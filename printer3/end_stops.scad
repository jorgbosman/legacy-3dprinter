// End stops to keep the rods in place
//
// jorg@bosman.tv - 2013-12-08

module end_stop() {
    difference() {
        union() {
            translate([0,0,-5]) cylinder(r=8, h=10, $fn=160);
            translate([0,0,-5]) cylinder(r=6, h=11, $fn=160);
            translate([7,0,0]) rotate([90,0,90]) cylinder(r=4, h=1, $fn=40);
        }
        translate([0,0,-7]) cylinder(r=4.2, h=15, $fn=80);
        translate([3,0,0]) rotate([90,0,90]) cylinder(r=3.5, h=3.5, $fn=6);
        translate([3,0,0]) rotate([90,0,90]) cylinder(r=1.8, h=7, $fn=18);
    }
}

end_stop();

