module bottom_frame() {
  difference() {
    union() {
      // outside plates
      translate([-2, -2, -2]) cube([2+15+15, 15+2, 2]);
      translate([-2, -2, -2]) cube([15+2, 2+15+15, 2]);
      translate([-2, -2, -2]) cube([2, 2+15+15, 15+2]);
      translate([-2, -2, -2]) cube([2+15+15, 2, 15+2]);
      translate([-2, -2, -2]) cube([15+2, 2, 2+30]);
      translate([-2, -2, -2]) cube([2, 15+2, 2+30]);
    }
    translate([15+21, 15+21, -3]) cylinder(r=7, h=4, $fn=70);
    translate([15+21, 15+21, 0]) cylinder(r=11.1, h=8, $fn=110);

    // nut holes
    translate([7.5, 7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 7.5+15, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15, 7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, -3, 7.5]) rotate([-90, 0, 0]) cylinder(r=3, h=4, $fn=18);
    translate([7.5+15, -3, 7.5]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 7.5, 7.5]) rotate([0, 90, 0]) cylinder(r=3, h=4, $fn=18);
    translate([-3, 7.5+15, 7.5]) rotate([0, 90, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 7.5, 7.5+15]) rotate([0, 90, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, -3, 7.5+15]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
  }
}

translate([0,0,0]) mirror([0,0,0]) bottom_frame();
translate([-10,0,0]) mirror([1,0,0]) bottom_frame();
translate([0,-10,0]) mirror([0,1,0]) bottom_frame();
translate([-10,-10,0]) mirror([1,1,0]) bottom_frame();

