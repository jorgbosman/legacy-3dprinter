module bearing2_frame() {
  difference() {
    union() {
      // outside plates
      translate([-2, 33, -2]) cube([2+15+2, 66, 2]);
      translate([-2, 33, -2]) cube([2, 66, 2+15]);
      translate([-2, 15+42+10, -2]) cube([2+15+21+13, 2+15, 2]);
      translate([15, 15+10+21-13, -2]) cube([2, 21+13, 2+15]);
      translate([15, 15+42+10-2, -2]) cube([21+13, 2, 2+15]);
      translate([15, 15+42+10+15, -2]) cube([21+13, 2, 2+15]);
      translate([15, 15+42+10+15, -2]) cube([2, 15+2, 2+15]);

      // bearing holder
      translate([15+21, 15+10+21, -2]) cylinder(r=13, h=7+2, $fn=130);
      translate([15, 15+10+21, -2]) cube([21+13, 21, 7+2]);
      translate([15, 15+10+21-13, -2]) cube([21, 21+13, 7+2]);
    }
    translate([15+21, 15+10+21, -3]) cylinder(r=7, h=4, $fn=70);
    translate([15+21, 15+10+21, 0]) cylinder(r=11.1, h=8, $fn=110);

    // nut holes
    translate([7.5, 33+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5-15, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5+15+2, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+21+13-7.5, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15+2, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 33+7.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5-15, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5, 7.5]) rotate([0,90,0]) cylinder(r=3, h=4, $fn=18);
    translate([-3, 15+10+42+7.5+15+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+10+42+7.5+15+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+7.5+2, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+21+13-7.5, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+21+13-7.5, 15+10+42-3, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+10+21-13+7.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

    // screwdriver space
    translate([15+21+13-7.5, 15+10, 7.5]) rotate([-90,0,0]) cylinder(r=4, h=40, $fn=40);
    translate([15+2, 15+10+21-13+7.5, 7.5]) rotate([0,90,0]) cylinder(r=4, h=40, $fn=40);
    translate([15+21+5, 15+21-3, 3.5]) cube([7,7,7]);
  }
}

translate([0,50,0]) mirror([0,1,0]) bearing2_frame();
//translate([60,-83,0]) mirror([0,0,0]) bearing2_frame();
//translate([0,-5,0]) mirror([0,0,0]) bearing2_frame();

