// Z Axis mounts
//
// jorg@bosman.tv - 2013-11-22

module 608z() {
  difference() {
    color("silver") cylinder(r=11, h=7);
    translate([0,0,-1]) cylinder(r=4, h=9);
  }
}

module z_axis_mount_bottom() {
  difference() {
    union() {
      translate([-21, -41, 7.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
      translate([-21,  41, 7.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
      translate([-21, -41, 0]) cube([2, 82, 15]);

      // Z-bottom
      translate([-21, -25.5-8, 0]) cube([21, 16, 15]);
      translate([  0, -25.5  , 0]) cylinder(r=8, h=15, $fn=80);

      translate([-21,  25.5-8, 0]) cube([21, 16, 15]);
      translate([  0,  25.5  , 0]) cylinder(r=8, h=15, $fn=80);
    }
    // Z-axis
    translate([0,-25.5,-1]) cylinder(r=4.2, h=17, $fn=40);
    translate([0, 25.5,-1]) cylinder(r=4.2, h=17, $fn=40);

    // holes for mounting to the frame
    translate([-22, -41, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([-22,  41, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);

    translate([-22, -10, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([-22,  10, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);

    // separating the 2 parts
    translate([-22, -1, -1]) cube([4, 2, 17]);
  }
}


module z_axis_mount1() {
  difference() {
    union() {
      translate([-21, -41-5.5, 7.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
      translate([-21,  41+2, 7.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
      translate([-21, -41-5.5, 0]) cube([2, 82+7.5, 15]);

      // Z-bottom
      translate([-21, -25.5-8-5.5, 0]) cube([21, 16, 15]);
      translate([  0, -25.5-5.5  , 0]) cylinder(r=8, h=15, $fn=80);

      translate([-21,  25.5-8+2, 0]) cube([21, 16, 15]);
      translate([  0,  25.5+2  , 0]) cylinder(r=8, h=15, $fn=80);

      translate([-21, -6, 0]) cube([14, 12, 15]);

      // holes for bolts on the outside
      translate([-3, -25.5-10-5.5, 7.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=40);
      translate([-3,  25.5+10+2, 7.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=40);
      translate([-3, -25.5-14-5.5, 0]) cube([3, 10, 15]);
      translate([-3,  25.5+2, 0]) cube([3, 14, 15]);
    }
    // Z-axis
    translate([0,-25.5-5.5,-1]) cylinder(r=4.2, h=17, $fn=42);
    translate([0, 25.5+2,-1]) cylinder(r=4.2, h=17, $fn=42);

    // allow for .5mm space between the 2 pieces
    translate([0-.25, -25.5-15-5.5, -1]) cube([9, 81+7.5, 17]);

    // holes for mounting to the frame
    translate([-22, -41-5.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([-22,  41+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);

    translate([-22, -11-5.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);
    translate([-22,  11+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=13, $fn=18);

    translate([-22, 0, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=31, $fn=18);
    translate([-22, 0, 7.5]) rotate([0,90,0]) cylinder(r=3.4, h=14, $fn=6);

    // holes for bolts on the outside
    translate([-4, -25.5-10-5.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);
    translate([-4,  25.5+10+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);

    // cutouts on the side for the nut and washer
    translate([-7, -25.5-10-5.5, 7.5]) rotate([0,90,0]) cylinder(r=4, h=4, $fn=40);
    translate([-7,  25.5+10+2, 7.5]) rotate([0,90,0]) cylinder(r=4, h=4, $fn=40);
  }
}


module z_axis_mount2() {
  difference() {
    union() {
      translate([6, -25.5-5.5, 0]) cube([2, 51+7.5, 15]);
      translate([3.5, 0, 7.5]) rotate([0,90,0]) cylinder(r=5, h=4, $fn=50);

      translate([0, -25.5-5.5, 0]) cylinder(r=8, h=15, $fn=80);
      translate([0,  25.5+2, 0]) cylinder(r=8, h=15, $fn=80);
      translate([0, -25.5-5.5, 0]) cube([8, 8, 15]);
      translate([0,  25.5-8+2, 0]) cube([8, 8, 15]);

      // holes for bolts on the outside
      translate([0, -25.5-10-5.5, 7.5]) rotate([0,90,0]) cylinder(r=4, h=8, $fn=40);
      translate([0,  25.5+10+2, 7.5]) rotate([0,90,0]) cylinder(r=4, h=8, $fn=40);
      translate([0, -25.5-10-5.5, 3.5]) cube([8, 10, 8]);
      translate([0,  25.5+2, 3.5]) cube([8, 10, 8]);
    }
    // Z-axis
    translate([0,-25.5-5.5,-1]) cylinder(r=4.2, h=17, $fn=42);
    translate([0, 25.5+2,-1]) cylinder(r=4.2, h=17, $fn=42);

    // allow for .5mm space between the 2 pieces
    translate([-9, -25.5-15-5.5, -1]) cube([9+.25, 81+7.5, 17]);

    translate([3, 0, 7.5]) rotate([0,90,0]) cylinder(r=4.2, h=2, $fn=42);

    // hole for 3mm bolt
    translate([3, 0, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=6, $fn=18);

    // holes for bolts on the outside
    translate([-1, -25.5-10-5.5, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=10, $fn=18);
    translate([-1,  25.5+10+2, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=10, $fn=18);
  }
}


module bearing_holder() {
    difference() {
        union() {
          translate([-21+14, 0, 7.5]) rotate([0,90,0]) cylinder(r=5, h=3.5, $fn=50);
          translate([-21+14, 0, 7.5]) rotate([0,90,0]) cylinder(r=3.9, h=12, $fn=40);
        }
        translate([-22+13, 0, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=28, $fn=18);
    }
}

module pulley_cover() {
  difference() {
    union() {
      cylinder(r=12.1, h=1.5+3.5, $fn=117); // main object
      cylinder(r=13.1, h=2, $fn=127); // lower edge
    }
    translate([0,0,1.5]) cylinder(r=11.1, h=1.5+3.5+2, $fn=110); // 608-ZZ ball bearing
    translate([0,0,-1]) cylinder(r=10, h=1.5+3.5+2, $fn=110); // 608-ZZ ball bearing
  }
}

module z_axis_mount() {
    z_axis_mount1();
    z_axis_mount2();
    bearing_holder();
}


module all_in_one() {
    z_axis_mount();
    translate([-3.5,0,7.5]) rotate([0,90,0]) 608z();
    translate([-5,0,7.5]) rotate([0,90,0]) pulley_cover();
    translate([ 5,0,7.5]) rotate([0,-90,0]) pulley_cover();
}

module print_bed() {
    //z_axis_mount1();
    //translate([12,0,8]) rotate([0,90,0]) z_axis_mount2();
    //translate([11,8,7]) rotate([0,-90,0]) bearing_holder();
    //translate([11,-8,7]) rotate([0,-90,0]) bearing_holder();
    //translate([60,0,0]) z_axis_mount_bottom();
    translate([0,0,0]) pulley_cover();
    translate([30,0,0]) pulley_cover();
}


//all_in_one();
mirror([0,1,0]) print_bed();

