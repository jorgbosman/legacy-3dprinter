// Prototype for Printer #5
//
// jorg@bosman.tv - 2013-06-23


// Printer dimensions

module motor1_frame() {
  difference() {
    union() {
      // outside plates
      translate([15, -2, -2]) cube([42-4, 2+15, 2]);
      translate([15, 15, -2]) cube([42, 42, 2]);
      translate([-2, -2, -2]) cube([17, 2+15+38, 2]);
      translate([-2, -2, -2]) cube([2, 2+15+38, 15+2]);
      translate([-2, -2, -2]) cube([2+15+42-4, 2, 15+2]);
      translate([-2, -2, -2]) cube([15+2, 2, 2+30]);
      translate([-2, -2, -2]) cube([2, 15+2, 2+30]);

      // inside plates
      translate([15, 15, -2]) cube([42-8, 2, 15+2]);
      translate([15, 15, -2]) cube([2, 42-8, 15+2]);

      // prettify
      translate([15, 15, -2]) cube([4, 4, 2+15]);
    }
    translate([15+21, 15+21, -3]) cylinder(r=12, h=4);

    // nut holes
    translate([7.5, 7.5, -3]) cylinder(r=3, h=4, $fn=18);
    translate([7.5, 53-7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([49.5-4, 7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, -3, 7.5]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([49.5-4, -3, 7.5]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 7.5, 7.5]) rotate([0, 90, 0]) cylinder(r=3, h=4, $fn=18);
    translate([-3, 7.5, 7.5+15]) rotate([0, 90, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 53-7.5, 7.5]) rotate([0, 90, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, -3, 7.5+15]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15, 7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15, -3, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 7.5+15, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 7.5+15, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

    // inner nut holes
    translate([15+21, 15-1, 7.5]) rotate([-90, 0, 0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+21, 7.5]) rotate([0, 90, 0]) cylinder(r=1.8, h=4, $fn=18);

    // motor nuts
    translate([15+5.5,15+5.5,-3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+5.5,15+36.5,-3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+36.5,15+5.5,-3]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+36.5,15+36.5,-3]) cylinder(r=1.8, h=4, $fn=18);

    //screwdriver space
    translate([15+5.5, 15+5.5, 0]) cylinder(r=4, h=16, $fn=40);

    // prettify
    translate([15+42, 15+42-4, -3]) rotate([0,0,45]) cube([4, 6, 4]);
    translate([15+42-4, 15, -3]) rotate([0,0,-45]) cube([4, 6, 4]);
    translate([15, 15+42-4, -3]) rotate([0,0,45]) cube([6, 4, 4]);
  }
}

mirror([0,1,0]) motor1_frame();

