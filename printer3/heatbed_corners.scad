module heatbed_corner() {
  difference() {
    union() {
      // outside plates
      translate([-2.1, 49.9, -2]) cube([2+15+2.2, 34, 2]);
      translate([-2.1, 49.9, -2]) cube([2, 34.2, 2+15]);
      translate([-2.1, 15+42+10-2.1, -2]) cube([2+15+15+2.2, 2+15+2.2, 2]);
      translate([15.2, 49.9, -2]) cube([2, 15+2, 2+15]);
      translate([15.2, 15+42+10-2.1, -2]) cube([15+2, 2, 2+15]);
      translate([-2.1, 15+42+10+15.1, -2]) cube([34.2, 2, 2+15]);
    }

    // nut holes
    translate([7.5, 15+10+42+7.5-15, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5+15+2, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5+15+2, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+7.5, -3]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5-15, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15-1, 15+10+42+7.5-15, 7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([-3, 15+10+42+7.5, 7.5]) rotate([0,90,0]) cylinder(r=3, h=4, $fn=18);
    translate([15+7.5+2, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([7.5, 15+10+42+15-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([15+7.5+2, 15+10+42+15-1-17, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);

    // screwdriver space
    translate([15+21+13-7.5, 15+10, 7.5]) rotate([-90,0,0]) cylinder(r=4, h=40, $fn=40);
    translate([15+2, 15+10+21-13+7.5, 7.5]) rotate([0,90,0]) cylinder(r=4, h=40, $fn=40);
    translate([15+21+5, 15+21-3, 3.5]) cube([7,7,7]);
  }
}

translate([5,-75,0]) mirror([0,0,0]) heatbed_corner();
translate([-5,-75,0]) mirror([1,0,0]) heatbed_corner();

