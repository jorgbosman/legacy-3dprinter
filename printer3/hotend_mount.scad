// Extruder for 2 e3d-online v5 hotends
//
// jorg@bosman.tv - 2013-11-05

module hotend() {
  translate([0,0,-1]) cylinder(r=4.8, h=10, $fn=40);
  translate([0,0, 1.8]) cylinder(r=8.8, h=2.1, $fn=80);
  translate([0,0, 5.5]) cylinder(r=8.8, h=3.6, $fn=80);
  translate([0,0, 7]) cylinder(r=6.6, h=8, $fn=60);
  translate([0,0,13.8]) cylinder(r=8.8, h=4.2, $fn=80);
  translate([0,0,16]) cylinder(r=10, h=25, $fn=100);
}

module hotend_mount_part1() {
  difference() {
    union() {
      translate([-12,-26,-12]) cube([30.5,52,15.5]);
      translate([-12,-26,-12]) cube([24,52,24]);
      translate([0,-26,12]) rotate([-90,0,0]) cylinder(r=12,h=52,$fn=120);
      translate([-12,0,0]) rotate([0,90,0]) cylinder(r=12,h=30.5,$fn=120);
    
      translate([-12, -28.5, -4.5]) cube([30.5, 4, 8]);
      translate([-12,  24.5, -4.5]) cube([30.5, 4, 8]);
      translate([-12, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=30.5,$fn=40);
      translate([-12,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=30.5,$fn=40);
    }
    // y-axis bushings
    translate([0,-27,12]) rotate([-90,0,0]) cylinder(r=8.2,h=54,$fn=80);
    translate([0,-30,12]) rotate([-90,0,0]) cylinder(r=9,h=4,$fn=80);
    translate([0, 26,12]) rotate([-90,0,0]) cylinder(r=9,h=4,$fn=80);

    // x-axis bushings
    translate([-13,0,0]) rotate([0,90,0]) cylinder(r=8.2,h=32.5,$fn=80);

    translate([19,-19,-12]) hotend();
    translate([19, 19,-12]) hotend();

    translate([-13, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=32.5,$fn=18);
    translate([-13,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=32.5,$fn=18);
  }
}

module hotend_mount_part2() {
  difference() {
    union() {
      translate([19,-26,-12]) cube([10,52,15.5]);
      translate([19,  0, 0]) rotate([0,90,0]) cylinder(r=12,h=10,$fn=120);
      translate([19, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=10,$fn=40);
      translate([19,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=4,h=10,$fn=40);
      translate([19, -28.5, -4.5]) cube([10, 4, 8]);
      translate([19,  24.5, -4.5]) cube([10, 4, 8]);
    }
    // x-axis busing
    translate([18,  0, 0]) rotate([0,90,0]) cylinder(r=8.2,h=12,$fn=120);

    translate([19,-19,-12]) hotend();
    translate([19, 19,-12]) hotend();

    translate([18, -28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=12,$fn=18);
    translate([18,  28.5, -0.5]) rotate([0,90,0]) cylinder(r=1.8,h=12,$fn=18);
  }
}

module hotend_mount() {
    hotend_mount_part1();
    translate([0,0,0]) hotend_mount_part2();
}

module print_bed() {
    translate([-10,0,0]) rotate([180,-90,0]) hotend_mount_part1();
    translate([30,0,17]) rotate([0, 90,0]) hotend_mount_part2();
}

module test_hotend_fit() {
  difference() {
    translate([0,0,30]) rotate([0, 90,0]) hotend_mount_part2();
    translate([-13,-7,-1]) cube([38,14,12]);
  }
}


//hotend_mount();
print_bed();
//test_hotend_fit();
