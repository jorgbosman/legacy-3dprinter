// Adjustable feet
//
// jorg@bosman.tv - 2013-11-30

module foot() {
    difference() {
        union() {
            cylinder(r=8, h=4, $fn=75);
        }
        translate([0,0,-1]) cylinder(r=3.4, h=3, $fn=6);
        translate([0,0,-1]) cylinder(r=1.8, h=6, $fn=18);
    }
}

module feet() {
    translate([7.5,7.5,-4]) foot();
    translate([FRAMESIZE_X-7.5,7.5,-4]) foot();
    translate([7.5,FRAMESIZE_Y-7.5,-4]) foot();
    translate([FRAMESIZE_X-7.5,FRAMESIZE_Y-7.5,-4]) foot();
}

module print_bed() {
    translate([-10,-10,4]) rotate([180,0,0]) foot();
    translate([-10, 10,4]) rotate([180,0,0]) foot();
    translate([ 10,-10,4]) rotate([180,0,0]) foot();
    translate([ 10, 10,4]) rotate([180,0,0]) foot();
}

print_bed();

