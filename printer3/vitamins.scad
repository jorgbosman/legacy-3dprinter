// All the non-printable parts
//
// jorg@bosman.tv - 2013-06-23


module motor() {
  color("silver") cube(size=[42, 42, 8]);
  color("black")  translate([0,0,8]) cube(size=[42, 42, 31]);
  color("silver") translate([0,0,31+8]) cube(size=[42, 42, 8]);
  color("silver") translate([21,21,31+8+8]) cylinder(r=11, h=2);

  color("silver") translate([5.5,5.5,47]) cylinder(r=1.5, h=7);
  color("silver") translate([5.5,36.5,47]) cylinder(r=1.5, h=7);
  color("silver") translate([36.5,5.5,47]) cylinder(r=1.5, h=7);
  color("silver") translate([36.5,36.5,47]) cylinder(r=1.5, h=7);
}

module coupler() {
  color("silver") cylinder(r=10, h=25);
}

module 608z() {
  difference() {
    color("silver") cylinder(r=11, h=7);
    translate([0,0,-1]) cylinder(r=4, h=9);
  }
}

module pulley() {
  color("silver") {
    difference() {
      union() {
          translate([0,0,-4.5]) cylinder(r=15, h=1);
          translate([0,0,-3.5]) cylinder(r=11, h=7);
          translate([0,0, 3.5]) cylinder(r=15, h=1);
      }
      translate([0,0,-5.5]) cylinder(r=4, h=11);
    }
  }
}

module timingbelt(length) {
  color("black") difference() {
    hull() {
      rotate([0,90,0]) cylinder(r=13, h=6);
      translate([0, length-42-30,0]) rotate([0,90,0]) cylinder(r=13, h=6);
    }
    hull() {
      translate([-1, 0, 0]) rotate([0,90,0]) cylinder(r=11, h=8);
      translate([-1, length-42-30, 0]) rotate([0,90,0]) cylinder(r=11, h=8);
    }
  }
}

module e3d_hotend() {
  color("silver") {
    translate([0,0,-1]) cylinder(r=4.8, h=10);
    translate([0,0, 1.8]) cylinder(r=8.8, h=2.1);
    translate([0,0, 5.5]) cylinder(r=8.8, h=3.6);
    translate([0,0, 7]) cylinder(r=6.6, h=8);
    translate([0,0,13.8]) cylinder(r=8.8, h=4.2);
    translate([0,0,-32]) cylinder(r=12.5, h=32);
    translate([0,0,-35]) cylinder(r=1.5, h=4);
    translate([-4, -8, -46]) cube([16, 16, 12]);
    translate([0,0,-48]) cylinder(r=4, h=3, $fn=6);
    translate([0,0,-50]) cylinder(r1=0, r2=2, h=2);
  }
  translate([-18,-15,-31]) color("red") cube([28,30,30]);
}

module psu() {
    color("lightgreen") {
        translate([-105, -50, -25]) cube([200, 100, 50]);
    }
}

module raspberrypi() {
    color("lightgreen") {
        translate([-50, -35, -15]) cube([100, 62, 30]);
    }
}

module arduino() {
    color("lightgreen") {
        translate([-55, -35, -35]) cube([110, 70, 70]);
    }
}

