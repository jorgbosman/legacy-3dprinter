// Prototype for Printer #3
//
// jorg@bosman.tv - 2013-06-23


// Printer dimensions

FRAMESIZE_X=410;
FRAMESIZE_Y=360;
FRAMESIZE_Z=372;
FRAME2_HEIGHT=89;

//color("red") cube([FRAMESIZE_X, 30, 30]);     // verify x-length
//color("red") cube([30, FRAMESIZE_Y, 30]);     // verify x-length
//color("red") cube([30,30,FRAMESIZE_Z]);       // verify z-length
//color("red") cube([30, 30, FRAMESIZE_Z-FRAME2_HEIGHT]);

// Animation voodoo
Xbedsize = 200;
Ybedsize = 200;
X=0;
Y=0;
Z=200;
//X=(Xbedsize/2)+sin($t*360)*(Xbedsize/2);
//Y=(Ybedsize/2)+cos($t*360)*(Ybedsize/2);
//Z=100-sin($t*360)*100;
Xreal=FRAMESIZE_X/2-(Xbedsize/2)+X;
Yreal=FRAMESIZE_Y/2-(Ybedsize/2)+Y;
Zreal=FRAMESIZE_Z-98-Z;

// Vitamins

use <vitamins.scad>;
use <motor1_frame.scad>;
use <motor2_frame.scad>;
use <bearing1_frame.scad>;
use <bearing2_frame.scad>;
use <axis_clamps.scad>;
use <hotend_mount.scad>;
use <heatbed.scad>;
use <heatbed_mount.scad>;
use <heatbed_belt_mount.scad>;
use <z_axis.scad>;
use <feet.scad>;
use <bottom_frame.scad>;
use <mid_frame.scad>;
use <electronics.scad>;
use <endstop_mounts.scad>;
use <extruders.scad>;

module frame() {
  color("silver") {
    // bottom
    translate([0, 0, 0]) cube([FRAMESIZE_X, 15, 15]); // 300
    translate([0, 15, 0]) cube([15, FRAMESIZE_Y-30, 15]); // 270
    translate([0, FRAMESIZE_Y-15, 0]) cube([FRAMESIZE_X, 15, 15]); // 300
    translate([FRAMESIZE_X-15, 15, 0]) cube([15, FRAMESIZE_Y-30, 15]); // 270

    // top
    translate([0, 0, FRAMESIZE_Z+15]) cube([FRAMESIZE_X, 15, 15]); // 300
    translate([0, 15, FRAMESIZE_Z+15]) cube([15, FRAMESIZE_Y-30, 15]); // 270
    translate([0, FRAMESIZE_Y-15, FRAMESIZE_Z+15]) cube([FRAMESIZE_X, 15, 15]); // 300
    translate([FRAMESIZE_X-15, 15, FRAMESIZE_Z+15]) cube([15, FRAMESIZE_Y-30, 15]); // 270

    // top2
    translate([0, 15, FRAMESIZE_Z+15-FRAME2_HEIGHT]) cube([15, FRAMESIZE_Y-30, 15]); // 270
    translate([FRAMESIZE_X-15, 15, FRAMESIZE_Z+15-FRAME2_HEIGHT]) cube([15, FRAMESIZE_Y-30, 15]); // 270

    // Z
    translate([0, 0, 15]) cube([15, 15, FRAMESIZE_Z]); // 300
    translate([FRAMESIZE_X-15, 0, 15]) cube([15, 15, FRAMESIZE_Z]); // 300
    translate([0, FRAMESIZE_Y-15, 15]) cube([15, 15, FRAMESIZE_Z]); // 300
    translate([FRAMESIZE_X-15, FRAMESIZE_Y-15, 15]) cube([15, 15, FRAMESIZE_Z]); // 300

    // Z-motor
    translate([0, FRAMESIZE_Y/2-36, 15]) cube([15,15,FRAMESIZE_Z-FRAME2_HEIGHT]);
    translate([FRAMESIZE_X-15, FRAMESIZE_Y/2-36, 15]) cube([15,15,FRAMESIZE_Z-FRAME2_HEIGHT]);
  }
}

module vitamins() {
  frame();
  translate([15, -49, FRAMESIZE_Z+15]) rotate([-90, 0, 0]) motor();
  translate([-49, 15, FRAMESIZE_Z+15-FRAME2_HEIGHT+57]) rotate([0, 90, 0]) motor();
  translate([15+21, FRAMESIZE_Y+15-22, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) 608z();
  translate([FRAMESIZE_X-15-21, 0, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) 608z();
  translate([FRAMESIZE_X-15-21, FRAMESIZE_Y+15-22, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) 608z();
  translate([7, FRAMESIZE_Y-15-21, FRAMESIZE_Z-FRAME2_HEIGHT+51]) rotate([-90,0,90]) 608z();
  translate([FRAMESIZE_X, FRAMESIZE_Y-15-21, FRAMESIZE_Z-FRAME2_HEIGHT+51]) rotate([-90,0,90]) 608z();
  translate([FRAMESIZE_X-7, 21+15, FRAMESIZE_Z-FRAME2_HEIGHT+51]) rotate([0,90,0]) 608z();

  translate([15+21, 6, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) coupler();

  translate([15+21, 36, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) pulley();
  translate([15+21, FRAMESIZE_Y-36, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) pulley();
  translate([FRAMESIZE_X-21-15, 36, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) pulley();
  translate([FRAMESIZE_X-21-15, FRAMESIZE_Y-36, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) pulley();

  translate([6, 15+21, FRAMESIZE_Z+15-FRAME2_HEIGHT+57-21]) rotate([0, 90, 0]) coupler();

  translate([36, 15+21, FRAMESIZE_Z+15-FRAME2_HEIGHT+57-21]) rotate([0, 90, 0]) pulley();
  translate([36, FRAMESIZE_Y-15-21, FRAMESIZE_Z+15-FRAME2_HEIGHT+57-21]) rotate([0, 90, 0]) pulley();
  translate([FRAMESIZE_X-36, 15+21, FRAMESIZE_Z+15-FRAME2_HEIGHT+57-21]) rotate([0, 90, 0]) pulley();
  translate([FRAMESIZE_X-36, FRAMESIZE_Y-15-21, FRAMESIZE_Z+15-FRAME2_HEIGHT+57-21]) rotate([0, 90, 0]) pulley();

  translate([15+21, 39, FRAMESIZE_Z-6]) rotate([0,0,-90]) timingbelt(FRAMESIZE_X);
  translate([15+21, FRAMESIZE_Y-33, FRAMESIZE_Z-6]) rotate([0,0,-90]) timingbelt(FRAMESIZE_X);
  translate([33, 15+21, FRAMESIZE_Z+15-FRAME2_HEIGHT+21+15]) timingbelt(FRAMESIZE_Y);
  translate([FRAMESIZE_X-39, 15+21, FRAMESIZE_Z+15-FRAME2_HEIGHT+21+15]) timingbelt(FRAMESIZE_Y);

  // z-motor
  //translate([-49, FRAMESIZE_Y/2-21, 42+15]) rotate([0, 90, 0]) motor();
  //translate([0, FRAMESIZE_Y/2, 21+15]) rotate([0, 90, 0]) coupler();
  //translate([0, FRAMESIZE_Y/2, 21+15]) rotate([0, 90, 0]) color("silver") cylinder(r=4, h=FRAMESIZE_X);
  //translate([FRAMESIZE_X-7, FRAMESIZE_Y/2, 36]) rotate([0,90,0]) 608z();

  //translate([36, FRAMESIZE_Y/2, 21+15]) rotate([0,90,0]) pulley();
  //translate([32, FRAMESIZE_Y/2, 21+15]) rotate([90,0,0]) timingbelt(FRAMESIZE_Z-FRAME2_HEIGHT+58);
  //translate([32, FRAMESIZE_Y/2, FRAMESIZE_Z-FRAME2_HEIGHT+22]) rotate([0,90,0]) 608z();

  //translate([FRAMESIZE_X-36, FRAMESIZE_Y/2, 21+15]) rotate([0,90,0]) pulley();
  //translate([FRAMESIZE_X-38, FRAMESIZE_Y/2, 21+15]) rotate([90,0,0]) timingbelt(FRAMESIZE_Z-FRAME2_HEIGHT+58);
  //translate([FRAMESIZE_X-39, FRAMESIZE_Y/2, FRAMESIZE_Z-FRAME2_HEIGHT+22]) rotate([0,90,0]) 608z();

  translate([15, FRAMESIZE_Y/2-49.5, 0]) motor();

  // cylinders
  color("silver") {
    translate([15+21, 0, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) cylinder(r=4, h=FRAMESIZE_Y, $fn=40);
    translate([FRAMESIZE_X-15-21, 0, FRAMESIZE_Z+15-21]) rotate([-90, 0, 0]) cylinder(r=4, h=FRAMESIZE_Y, $fn=40);
    translate([0, 15+21, FRAMESIZE_Z+15-21-FRAME2_HEIGHT+57]) rotate([0, 90, 0]) cylinder(r=4, h=FRAMESIZE_X, $fn=40);
    translate([0, FRAMESIZE_Y-15-21, FRAMESIZE_Z+15-21-FRAME2_HEIGHT+57]) rotate([0, 90, 0]) cylinder(r=4, h=FRAMESIZE_X, $fn=40);
    translate([25, Yreal, FRAMESIZE_Z-16]) rotate([0,90,0]) cylinder(r=4, h=FRAMESIZE_X-50, $fn=40);
    translate([Xreal, 25, FRAMESIZE_Z-FRAME2_HEIGHT+61]) rotate([-90,90,0]) cylinder(r=4, h=FRAMESIZE_Y-50, $fn=40);

    // Z
    #translate([36,FRAMESIZE_Y/2-28.5,0]) cylinder(r=4, h=FRAMESIZE_Z-FRAME2_HEIGHT+30, $fn=40);
    translate([36,FRAMESIZE_Y/2+27.5,0]) cylinder(r=4, h=FRAMESIZE_Z-FRAME2_HEIGHT+30, $fn=40);
    translate([FRAMESIZE_X-36,FRAMESIZE_Y/2-31,0]) cylinder(r=4, h=FRAMESIZE_Z-FRAME2_HEIGHT+30, $fn=40);
    translate([FRAMESIZE_X-36,FRAMESIZE_Y/2+27.5,0]) cylinder(r=4, h=FRAMESIZE_Z-FRAME2_HEIGHT+30, $fn=40);
  }

  // extruders
  translate([FRAMESIZE_X/2-45,-56,FRAMESIZE_Z+5]) rotate([-90,25,0]) motor();
  translate([FRAMESIZE_X/2+75,-56,FRAMESIZE_Z+5]) rotate([-90,25,0]) motor();
}

module axis_clamps() {
  translate([15+21, Yreal, FRAMESIZE_Z-6]) rotate([180,0,90]) xy_clamp();
  translate([FRAMESIZE_X-15-21, Yreal, FRAMESIZE_Z-6]) rotate([180,0,90]) xy_clamp();
  translate([Xreal, FRAMESIZE_Y-15-21, FRAMESIZE_Z-FRAME2_HEIGHT+51]) rotate([0,0,0]) xy_clamp();
  translate([Xreal, 15+21, FRAMESIZE_Z-FRAME2_HEIGHT+51]) rotate([0,0,0]) xy_clamp();
}

module xy_axis() {
  translate([0, 0, FRAMESIZE_Z+30]) rotate([-90, 0, 0]) motor1_frame();
  translate([0, 0, FRAMESIZE_Z+30-FRAME2_HEIGHT+67]) mirror([1,0,0]) rotate([-90, 0, 90]) motor2_frame();
  translate([0, FRAMESIZE_Y, FRAMESIZE_Z+30]) mirror([0,1,0]) rotate([-90, 0, 0]) bearing1_frame();
  translate([FRAMESIZE_X, 0, FRAMESIZE_Z+30]) mirror([1,0,0]) rotate([-90, 0, 0]) bearing1_frame();
  translate([FRAMESIZE_X, FRAMESIZE_Y, FRAMESIZE_Z+30]) rotate([-90, 0, 180]) bearing1_frame();
  translate([FRAMESIZE_X, 0, FRAMESIZE_Z+30-FRAME2_HEIGHT+67]) rotate([-90,0,90]) bearing2_frame();
  translate([FRAMESIZE_X, FRAMESIZE_Y, FRAMESIZE_Z+30-FRAME2_HEIGHT+67]) mirror([1,0,0]) rotate([-90,0,-90]) bearing2_frame();
  translate([0, FRAMESIZE_Y, FRAMESIZE_Z+30-FRAME2_HEIGHT+67]) rotate([0,0,180]) rotate([-90,0,90]) bearing2_frame();
}


module hotend_carriage() {
  translate([Xreal, Yreal, FRAMESIZE_Z-28]) rotate([0,0,90]) hotend_mount();
  translate([Xreal-19, Yreal+19, FRAMESIZE_Z-28-12]) rotate([0,0,90]) e3d_hotend();
  translate([Xreal+19, Yreal+19, FRAMESIZE_Z-28-12]) rotate([0,0,90]) e3d_hotend();
}

module bed_mount() {
  difference() {
    union() {
      translate([0,0,0]) cylinder(r=8, h=15, $fn=80);
    }
  }
}

module z_axis() {
  translate([36, FRAMESIZE_Y/2-28.5, -2]) z_motor_bottom();
  translate([36, FRAMESIZE_Y/2-28.5, 47]) z_motor_top();
  translate([-2, FRAMESIZE_Y/2-28.5, 15+7.5]) rotate([90,0,90]) z_frame();
  translate([FRAMESIZE_X-36, FRAMESIZE_Y/2-28.5, -2]) mirror([1,0,0]) z_motor_bottom();
  translate([FRAMESIZE_X-36, FRAMESIZE_Y/2-28.5, 47]) mirror([1,0,0]) z_motor_top();
  translate([FRAMESIZE_X+2, FRAMESIZE_Y/2-28.5, 15+7.5]) mirror([1,0,0]) rotate([90,0,90]) z_frame();

  // bottom frame
  //translate([0, FRAMESIZE_Y/2+46, 0]) rotate([0,0,180]) rotate([0,-90,0]) motor2_frame();
  //translate([FRAMESIZE_X, FRAMESIZE_Y/2+46, 0]) mirror([1,0,0]) rotate([0,0,180]) rotate([0,-90,0]) bearing2_frame();

  // heatbed frame
  translate([FRAMESIZE_X/2+169,FRAMESIZE_Y/2+2,Zreal]) mirror([1,0,0]) z_mount();
  translate([FRAMESIZE_X/2-169,FRAMESIZE_Y/2+2,Zreal]) z_mount();
  //translate([FRAMESIZE_X/2+169,FRAMESIZE_Y/2-45,Zreal-16]) rotate([0,0,-90]) z_belt_mount();
  //translate([FRAMESIZE_X/2-169,FRAMESIZE_Y/2-45,Zreal-16]) rotate([0,0,90]) mirror([1,0,0]) z_belt_mount();
}

module feet() {
    translate([7.5,7.5,-4]) foot();
    translate([FRAMESIZE_X-7.5,7.5,-4]) foot();
    translate([7.5,FRAMESIZE_Y-7.5,-4]) foot();
    translate([FRAMESIZE_X-7.5,FRAMESIZE_Y-7.5,-4]) foot();
}

module electronics() {
    translate([15, 17, 37]) rotate([0,-90,0]) psu();
    translate([15+2, 0, 0]) rotate([0,-90,0]) psu_bottom();
    translate([-28, 17, 47]) rotate([0,90,0]) psu_lid();
    translate([17, 0, 209.5]) rotate([0,-90,0]) psu_top();
}

module endstops() {
    translate([15,50,FRAMESIZE_Z+15]) rotate([-90,0,180]) endstop_y();
    translate([60,15,FRAMESIZE_Z+15]) rotate([-90,0,-90]) endstop_x();
}

vitamins();
translate([FRAMESIZE_X/2, FRAMESIZE_Y/2, Zreal]) heatbed();
//axis_clamps();
//xy_axis();
//hotend_carriage();
z_axis();
//electronics();
//feet();
//extruders();
//endstops();

