// Endstop mounts
//
// jorg@bosman.tv - 2014-01-01

module endstop_x() {
    difference() {
        union() {
            translate([0,0,0]) cube([15, 2, 26]);
            cube([2, 54, 14]);
            cube([15, 15, 2]);
            translate([0,0,12]) cube([15, 15, 2]);

            translate([0, 39, 7]) rotate([0,90,0]) cylinder(r=4, h=2.5, $fn=40);
            translate([0, 49, 7]) rotate([0,90,0]) cylinder(r=4, h=2.5, $fn=40);
        }

        // triangle
        translate([15, 2, -1]) rotate([0,0,45]) cube([10, 18.38, 4]);
        translate([15, 2, 11]) rotate([0,0,45]) cube([10, 18.38, 4]);

        translate([7.5, -1, 7]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([7.5, -1, 7+12]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-1, 39, 7]) rotate([0,90,0]) cylinder(r=1.8, h=6, $fn=18);
        translate([-1, 49, 7]) rotate([0,90,0]) cylinder(r=1.8, h=6, $fn=18);
    }
}

module endstop_y() {
    difference() {
        union() {
            translate([0,0,-12]) cube([15, 2, 14]);
            translate([0,0,12]) cube([15, 2, 14]);
            cube([2, 20, 14]);
            cube([15, 15, 2]);
            translate([0,0,12]) cube([15, 15, 2]);

            translate([0, 5, 7]) rotate([0,90,0]) cylinder(r=4, h=2.5, $fn=40);
            translate([0, 15, 7]) rotate([0,90,0]) cylinder(r=4, h=2.5, $fn=40);
        }

        // triangle
        translate([15, 2, -1]) rotate([0,0,45]) cube([10, 18.38, 4]);
        translate([15, 2, 11]) rotate([0,0,45]) cube([10, 18.38, 4]);

        translate([7.5, -1, 7-12]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([7.5, -1, 7+12]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-1, 5, 7]) rotate([0,90,0]) cylinder(r=1.8, h=6, $fn=18);
        translate([-1, 15, 7]) rotate([0,90,0]) cylinder(r=1.8, h=6, $fn=18);
    }
}

module endstop_z() {
    difference() {
        union() {
            translate([-10,0,0]) cube([25,15,2]);
            translate([-0.1,0,0]) cube([15+4.2,15+2+30,2]);
            translate([-0.1,17,0]) cube([2,30,2+15]);
            translate([17.1,17,0]) cube([2,30,2+15]);
        }
        #translate([-1,17+7.5,2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        #translate([-1,17+15+7.5,2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        #translate([16,17+7.5,2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        #translate([16,17+15+7.5,2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

        translate([-4,7.5,-1]) cylinder(r=1.8,h=4,$fn=18);
        translate([-4,7.5,1]) cylinder(r=3,h=2,$fn=18);
        translate([6,7.5,-1]) cylinder(r=1.8,h=4,$fn=18);
        translate([6,7.5,1]) cylinder(r=3,h=2,$fn=18);
    }
}

//mirror([1,0,0]) rotate([0,-90,0]) endstop_x();
//translate([60,0,0]) rotate([0,-90,0]) endstop_y();
mirror([1,0,0]) endstop_z();
