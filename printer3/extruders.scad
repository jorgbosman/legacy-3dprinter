// The extruders
// For now based on existing parts
//
// jorg@bosman.tv - 2014-01-04

use <externals/gregs-wade-v3.scad>;
use <vitamins.scad>;

module extruder_body() {
    difference() {
        union() {

            // modified extruder
            difference() {
                union() {
                    wade(0);
                    translate([20,7,0]) cube([18,17,12]);
                    translate([36,18,0]) cube([50,32,12]);
                    translate([49,7,0]) cube([27,12,12]);
                    translate([47,7,0]) cube([5,3.5,12]);
                    translate([36,7,0]) cube([4,3.5,12]);
                    translate([45,19,0]) rotate([0,0,-45]) cube([7,3.5,12]);
                    translate([59,58.5,0]) rotate([0,0,-20]) cube([30,2,12]);
                    translate([63,57,0]) rotate([0,0,-70]) cube([3,2,12]);
                    translate([62,50,0]) rotate([0,0,-20]) cube([20,8,12]);
                }
                translate([-30,-1,-1]) cube([80,8.1,40]);
                translate([24,6,12]) cube([30,8,20]);
                translate([64,-8,-1]) rotate([0,0,-20]) cube([15,66,14]);
            }
            translate([16,10,0]) rotate([0,0,-20]) cube([51.3,8,12]);
            translate([45,4,0]) rotate([0,0,-20]) cube([22,15,12]);
        }

        // reduce the height of the motor mounting part
        translate([24,-9,9]) cube([65,72,4]);

        // holes for mounting to the frame
        translate([61.03,-0.75,6]) cylinder(r=4, h=4, $fn=18);
        translate([61.03,-0.75,-1]) cylinder(r=1.8, h=14, $fn=18);

        translate([78.65,47.79,6]) cylinder(r=4, h=4, $fn=18);
        translate([78.65,47.79,-1]) cylinder(r=1.8, h=14, $fn=18);

        // space for the motor
        translate([51, 34, -1]) cylinder(r=15, h=14, $fn=110);
        translate([45.5,20, -1]) rotate([0,0,-20]) cube([28,30,14]);

        // hole for bowden connector
        translate([7.3,7.1-1,14]) rotate([-90,0,0]) cylinder(r=4.9, h=8+1, $fn=60);
        translate([7.3,7.1+0-1,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5+1, $fn=60);
        translate([7.3,7.1+1,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+2,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+3,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+4,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+5,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+6,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
        translate([7.3,7.1+7,14]) rotate([-90,0,0]) cylinder(r=5, h=0.5, $fn=60);
    }
}

module all_in_one() {
    extruder_body();
    //color("silver") translate([67,0,-15]) rotate([0,0,70]) cube([60,15,15]);
    translate([79,23,59]) rotate([0,180,24]) motor();
}

module printbed() {
    extruder_body();
}

module test_bowden_connector() {
    difference() {
        extruder_body();
        translate([-20,0,-1]) cube([20,60,30]);
        translate([ 15,-10,-1]) cube([80,80,30]);
        translate([-1, 20,-1]) cube([30,40,30]);
        translate([-1, 0, -1]) cube([30,30,8]);
        translate([-1, 0, 21]) cube([30,30,8]);
    }
}

mirror([0,1,0]) printbed();
//all_in_one();
//test_bowden_connector();

