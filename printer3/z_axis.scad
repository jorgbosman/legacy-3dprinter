// Z Axis mounts
//
// jorg@bosman.tv - 2014-02-11

module z_motor_bottom() {
    difference() {
        union() {
            translate([-2-21-15,-23,0]) cube([2+15+44,46,2]);
            translate([-2-21-15-.1,-7.5-30,0]) cube([2,30+15+30,2+15]);
            translate([-2-21-15-.1,-7.5-30,0]) cube([2+15+.2,30+15+30,2]);
            translate([-21+.1, -7.5-30, 0]) cube([2, 16.5, 2+15]);
            translate([-21+.1,  21, 0]) cube([2, 16.5, 2+15]);

            translate([-20, -23, 0]) cube([43, 46, 7.5]);
        }
        translate([-21, -21.1, 2]) cube([42, 42.2, 7.5]);

        translate([-15.5,-15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 15.5,-15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([-15.5, 15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 15.5, 15.5,-1]) cylinder(r=1.8, h=4, $fn=18);

        translate([-21-7.5, -7.5-15-7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([-21-7.5,  7.5+15+7.5, -1]) cylinder(r=1.8, h=4, $fn=18);

        translate([-21-15-3, -7.5-15-7.5, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-21-15-3,  7.5+15+7.5, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-21-1, -7.5-15-7.5, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-21-1,  7.5+15+7.5, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
    }
}

module z_motor_top() {
    difference() {
        union() {
            translate([-21,-21,0]) cube([42,42,2]);
            translate([-21-15, -7.5-2.1, 0]) cube([17,2,15]);
            translate([-21-15, 7.5+.1, 0]) cube([17,2,15]);
            translate([-21, -7.5-2-.1, 0]) cube([9,15+4.2, 15]);
        }

        translate([-12, -7.5-3, 2]) rotate([0,-29,0]) cube([9,15+6, 15]);

        translate([0,0,-1]) cylinder(r=12, h=4, $fn=120);
        translate([-15.5,-15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 15.5,-15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([-15.5, 15.5,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 15.5, 15.5,-1]) cylinder(r=1.8, h=4, $fn=18);

        translate([-21-7.5, -7.5-3, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-21-7.5, 7.5-1, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    }
}

module z_frame() {
    difference() {
        union() {
            translate([-7.5-2, -7.5, 0]) cube([15+4,15,2]);
            translate([-7.5-30-.1, -7.5, 0]) cube([30, 2, 15+2]);
            translate([7.5+.1, -7.5, 0]) cube([30, 2, 15+2]);
            translate([-7.5-2-.1, -7.5, 0]) cube([2, 15, 15+2]);
            translate([ 7.5+.1, -7.5, 0]) cube([2, 15, 15+2]);
        }
        translate([0,0,-1]) cylinder(r=1.8, h=4, $fn=18);
        translate([-7.5-3,0,7.5+2]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 7.5-1,0,7.5+2]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([-7.5-15-7.5,-7.5-1,7.5+2]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([ 7.5+15+7.5,-7.5-1,7.5+2]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    }
}

module print_bed() {
    translate([0,0,0]) z_motor_bottom();
    translate([5,-50,0]) z_motor_top();
    translate([-5,35,0]) rotate([0,0,180]) z_frame();
}


//all_in_one();
mirror([0,1,0]) print_bed();

