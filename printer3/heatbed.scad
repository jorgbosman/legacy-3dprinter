// 4 Clamps to secure the Glassbed to the PCB Heatbed
//
// jorg@bosman.tv - 2012-02-17

module vitamins() {
    translate([-105,-100,2]) color("lightcoral") cube([210,200,6]); // Glass plate
    translate([-107,-107,0]) color("red") cube([214,214,2]); // PCB Heatbed

    color("silver") {
        translate([-142, -82-15, -16]) cube([284, 15, 15]);
        translate([-142,  82, -16]) cube([284, 15, 15]);
        translate([-157, -97, -16]) cube([15, 194, 15]);
        translate([ 157-15, -97, -16]) cube([15, 194, 15]);
    }
}

module glass_clamp() {
    difference() {
        union() {
            translate([-3.5,-3.5,-1]) cube([5,5,10]);
            translate([ 1.5, 1.5,-1]) cylinder(r=5, h=10, $fn=60);
        }
        translate([-0.5, 4.5,1]) cube([8,3,6+1]); // Glass plate
        translate([-2.5,-2.5,0]) cube([10,10,2]); // PCB Heatbed
        translate([0,0,-2]) cylinder(r=1.8, h=12, $fn=18);
    }
}

module frame_mount() {
    difference() {
        union() {
            translate([-7.5, -7.5, 0]) cube([15, 15, 15]);
            translate([-7.5, 7.5, 6.2]) cube([15, 3, 2.6]);
        }
        hull() {
            translate([0, -1, -1]) cylinder(r=1.85, h=17, $fn=18);
            translate([0, 1, -1]) cylinder(r=1.85, h=17, $fn=18);
        }
        translate([0, 5.5+0.2, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=6, $fn=18);
        translate([0, -8.5, 7.5]) rotate([-90,0,0]) cylinder(r=3.5, h=14, $fn=30);

        translate([0, -1, -1]) rotate([0,0,90]) cylinder(r=3.5, h=3, $fn=6);
        translate([0, 1, -1]) rotate([0,0,90]) cylinder(r=3.5, h=3, $fn=6);
        translate([-2, -2.94, -1]) cube([4, 5.88, 3]);
    }
}

module old_z_mount() {
    difference() {
        union() {
            translate([0, -25.5, -16]) cylinder(r=12, h=22, $fn=120);
            translate([0,  25.5, -16]) cylinder(r=12, h=15, $fn=120);
            translate([0, -37.5, -16]) cube([12, 24, 15]);
            translate([0,  13.5, -16]) cube([12, 24, 15]);

            translate([10, -42, -16]) cube([2, 84, 15]);
            translate([10, -42, -8.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
            translate([10,   0, -8.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
            translate([10,  42, -8.5]) rotate([0,90,0]) cylinder(r=7.5, h=2, $fn=75);
        }
        translate([0, -25.5, -17]) cylinder(r=8.2, h=24, $fn=40);
        translate([0,  25.5, -17]) cylinder(r=8.2, h=24, $fn=40);

        // holes for the bolts
        translate([9, -42, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([9, -8, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([9,  8, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([9,  42, -8.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

        // split the items in two
        translate([9, -1, -17]) cube([4, 2, 17]);
    }
}

module heatbed() {
    vitamins();
    translate([-104.5, -104.5, 0]) glass_clamp();
    translate([ 104.5, -104.5, 0]) mirror([1,0,0]) glass_clamp();
    translate([-104.5,  104.5, 0]) mirror([0,1,0]) glass_clamp();
    translate([ 104.5,  104.5, 0]) mirror([1,0,0]) mirror([0,1,0]) glass_clamp();

    translate([ 0, -104.5, -16]) mirror([1,0,0]) frame_mount();
    translate([-104.5,  104.5, -16]) mirror([0,1,0]) frame_mount();
    translate([ 104.5,  104.5, -16]) mirror([1,0,0]) mirror([0,1,0]) frame_mount();

    //translate([-205+36, 0, 0]) mirror([0,0,0]) z_mount();
    //translate([ 205-36, 0, 0]) mirror([1,0,0]) z_mount();
}

module print_bed() {
    //translate([0,0,3.5]) rotate([90,0,0]) mirror([0,0,0]) glass_clamp();
    //translate([20,0,3.5]) rotate([90,0,0]) mirror([1,0,0]) glass_clamp();
    //translate([0,20,3.5]) rotate([-90,0,0]) mirror([0,1,0]) glass_clamp();
    //translate([20,20,3.5]) rotate([-90,0,0]) mirror([1,0,0]) mirror([0,1,0]) glass_clamp();

    translate([40,5,16]) z_mount();
    //translate([70,5,16]) mirror([1,0,0]) z_mount();
    //translate([40,5,7.5]) rotate([90,0,0]) frame_mount();
    //translate([40,30,7.5]) rotate([90,0,0]) frame_mount();
    //translate([60,5,7.5]) rotate([90,0,0]) frame_mount();
}

print_bed();
//heatbed();

