// Case for the electronics
// Power Supply
// Raspberry pi + Camera
// Arduino + Shield + Stepperdrivers
//
// jorg@bosman.tv - 2013-12-15

module frame() {
    color("silver") {
        translate([0,0,0]) cube([15, 140, 15]);
        translate([0,0,0]) cube([100, 15, 15]);
        translate([0,0,0]) cube([15, 15, 280]);
    }
}

module psu() {
    color("grey") {
        difference() {
            union() {
                translate([0,0,0]) cube([200, 98, 1.5]);
                translate([0,0,0]) cube([200, 1.5, 43]);
                translate([10, 0, 0]) cube([185, 98, 43]);
                translate([195, 0, 0]) cube([5, 85, 8]);
                translate([200, 0, 8]) rotate([0, -180+45, 0]) cube([7, 85, 5]);

                translate([-1, 20, 9]) cube([15, 68, 6]);
                translate([-1, 20, 25]) cube([15, 68, 2]);
            }
            translate([3, 6, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([200-4.5, 98-7, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([200-4.5, 98-8.5, -1]) cube([5, 3, 4]);
            translate([200-3.5, -1, 28]) rotate([-90,0,0]) cylinder(r=1.5, h=4, $fn=15);
            translate([200-3.5, -1, 28-1.5]) cube([4, 4, 3]);
            translate([6.5, -1, 30]) rotate([-90,0,0]) cylinder(r=1.5, h=4, $fn=15);
            translate([5, 98-5, -1]) cylinder(r=1.5, h=4, $fn=15);

            translate([61, 9, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([61, 89, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([200-12, 9, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([200-12, 89, -1]) cylinder(r=1.5, h=4, $fn=15);
            translate([23, -1, 22]) rotate([-90,0,0]) cylinder(r=1.5, h=4, $fn=15);
            translate([180, -1, 11]) rotate([-90,0,0]) cylinder(r=1.5, h=4, $fn=15);
            translate([180, -1, 29]) rotate([-90,0,0]) cylinder(r=1.5, h=4, $fn=15);
        }
    }
    
}

module psu_top() {
    difference() {
        union() {
            cube([32, 15+2+98, 2]);
            translate([0, 15, 0]) cube([32, 2, 43+2]);
            translate([17,0,15+2.1]) cube([15, 15, 2]);
            translate([30,15,0]) cube([2,100,45]);
            translate([23,15,43]) cube([8, 100, 2]);
            translate([23,113,0]) cube([9,2,45]);
        }
        translate([22,17,42]) cube([6,2,4]);
        translate([22,112,2]) cube([6,4,2]);

        translate([7.5, 14, 31]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([15.5, 26, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([15.5, 106, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([17+7.5, 7.5, 16]) cylinder(r=1.8, h=4, $fn=18);
        translate([7.5, 7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([17+7.5, 7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
    }
}

module psu_bottom() {
    difference() {
        union() {
            translate([15, 0, 0]) cube([83+7.5, 15+2+98, 2]);
            translate([0, 15, 0]) cube([15+1, 98+2, 2]);
            translate([15, 15, 0]) cube([83+7.5, 2, 43+2]);
            translate([15, 15, 0]) cube([2, 2+98, 2+15]);
            translate([15, 15+2+98-2, 0]) cube([21, 2, 17]);
        }
        translate([32+15,32,-1]) cube([73.5-30,82-15,4]);
        translate([106,14,17]) rotate([0,-45,0]) cube([20,4,40]);

        translate([15+83, 7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+83, 15+2+9, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+83, 15+2+89, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+2+43, 15-1, 15+2+7]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+7.5, 7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([7.5, 15+7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([7.5, 15+2+98-7.5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([40, 23, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([43.5, 14, 32]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+2+20+5, 15+2+98-5, -1]) cylinder(r=1.8, h=4, $fn=18);
        translate([15+2+7.5, 15-1, 2+7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([15-1, 15+2+7.5, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);
        translate([15-1, 15+2+98-7.5-2, 2+7.5]) rotate([0,90,0]) cylinder(r=1.8, h=4, $fn=18);

        // power cable
        translate([17+15.25, 15-1, 17+15]) rotate([-90,0,0]) cylinder(r=3, h=4, $fn=30);

        translate([17+4, 15-1, 17+15]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=30);
        translate([25, 15+2+98-3, 2+7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=30);
    }
}

module psu_lid() {
    difference() {
        union() {
            translate([0,0.5,0]) cube([32,97.5,2]);
            translate([30,0.5,0]) cube([2,97.5,28]);
            translate([0,98-2,0]) cube([32,2,28]);

            translate([11,98-4.5,0]) cube([21,2.5,41]);
            translate([18,0.5,0]) cube([12,2,30]);
        }
        translate([-1,-1,-1]) cube([12, 3, 4]);
        translate([11,98-2.5,28]) cube([22,4,15]);

        translate([22,98-5,35.5]) rotate([-90,0,0]) cylinder(r=1.8, h=6, $fn=18);
        translate([26,-3,13]) rotate([-90,0,0]) cylinder(r=1.8, h=6, $fn=18);
    }
}

module printbed() {
    //mirror([0,1,0]) psu_bottom();
    //mirror([0,1,0]) psu_lid();
    mirror([0,1,0]) rotate([0,90,0]) psu_top();
}

module all_in_one() {
    frame();
    translate([15, 17, 37]) rotate([0,-90,0]) psu();
    translate([15+2, 0, 0]) rotate([0,-90,0]) psu_bottom();
    translate([-28, 17, 47]) rotate([0,90,0]) psu_lid();
    translate([17, 0, 217-7.5]) rotate([0,-90,0]) psu_top();
}

printbed();
//all_in_one();

