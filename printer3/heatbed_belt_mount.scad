// Prototype for Printer #3
//
// jorg@bosman.tv - 2013-06-23


// Printer dimensions

module clamp_bottom() {
  difference() {
    union() {
      // main cylinder
      cylinder(r=12, h=28, $fn=120);

      // top part
      translate([-19, -5, 0]) cube([10,10,28]);
      hull() {
        translate([-19, -6, 3.5]) rotate([0,90,0]) cylinder(r=3.5, h=3, $fn=35);
        translate([-19,  6, 3.5]) rotate([0,90,0]) cylinder(r=3.5, h=3, $fn=35);
        translate([-19, -6, 24.5]) rotate([0,90,0]) cylinder(r=3.5, h=3, $fn=35);
        translate([-19,  6, 24.5]) rotate([0,90,0]) cylinder(r=3.5, h=3, $fn=35);
      }

      // side plate
      translate([19.5, -12, 7.5]) rotate([-90,0,0]) cylinder(r=7.5, h=2, $fn=75);
      translate([-32, -12, 0]) cube([51.5, 2, 15]);
      translate([-19, -12, 0]) cube([3, 12, 15]);
    }

    // holes for smooth rods
    translate([0,0,-1]) cylinder(r=8.2, h=30, $fn=80);

    // holes in top plate
    translate([-20, -6, 3.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);
    translate([-20,  6, 3.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);
    translate([-20, -6, 24.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);
    translate([-20,  6, 24.5]) rotate([0,90,0]) cylinder(r=1.8, h=5, $fn=18);

    // extra space for bolts under top plate
    translate([-16, -6, 3.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=35);
    translate([-16,  6, 3.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=35);
    translate([-16, -6, 24.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=35);
    translate([-16,  6, 24.5]) rotate([0,90,0]) cylinder(r=4, h=3, $fn=35);

    // holes in side plate
    translate([-26.5, -13, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
    translate([ 19.5, -13, 7.5]) rotate([-90,0,0]) cylinder(r=1.8, h=4, $fn=18);
  }
}

module clamp_bottom_with_endstop() {
  difference() {
    union() {
      mirror([0,1,0]) clamp_bottom();
      translate([-10,-17,0]) cube([10,12,10]);
    }
    translate([0,0,-1]) cylinder(r=8.2, h=30, $fn=80);

    translate([-5,-14.4,2.2]) cylinder(r=1.8, h=8,$fn=18);
    translate([-5,-14.4,-1]) cylinder(r=3.3, h=3, $fn=6);
    translate([-5,-14.4,8]) cylinder(r=3.3, h=3, $fn=6);
  }
}

module belt_clamp1() {
  difference() {
    union() {
      hull() {
        translate([-6, 0, 0]) cylinder(r=3.5, h=9.5, $fn=35);
        translate([ 6, 0, 0]) cylinder(r=3.5, h=9.5, $fn=35);
      }
      hull() {
        translate([-6, -21, 0]) cylinder(r=3.5, h=3, $fn=35);
        translate([ 6, -21, 0]) cylinder(r=3.5, h=3, $fn=35);
      }
      translate([-9.5, -21, 0]) cube([ 6, 21, 3]);
      translate([ 3.5, -21, 0]) cube([ 6, 21, 3]);
    }
    translate([-6, 0, -1]) cylinder(r=1.8, h=12, $fn=18);
    translate([ 6, 0, -1]) cylinder(r=1.8, h=12, $fn=18);

    translate([0, -5, 5]) rotate([-90,0,0]) cylinder(r=1.8, h=10, $fn=18);
    translate([0, -3.5, 5]) rotate([-90,0,0]) cylinder(r=3.3, h=2, $fn=6);
    translate([-6, -21, -1]) cylinder(r=1.8, h=5, $fn=18);
    translate([ 6, -21, -1]) cylinder(r=1.8, h=5, $fn=18);

    translate([-3.5, -5, -1]) cube([7, 10, 2.5]);

    translate([-3.5, -25.5, -1]) cube([7, 2, 2]);
    translate([-3.5, -22.5, -1]) cube([7, 1, 2]);
    translate([-3.5, -20.5, -1]) cube([7, 1, 2]);
    translate([-3.5, -18.5, -1]) cube([7, 2, 2]);
  }
}

module belt_clamp2() {
  difference() {
    hull() {
      translate([-6, 0, 0]) cylinder(r=3.5, h=3, $fn=35);
      translate([ 6, 0, 0]) cylinder(r=3.5, h=3, $fn=35);
    }
    translate([-6, 0, -1]) cylinder(r=1.8, h=5, $fn=18);
    translate([ 6, 0, -1]) cylinder(r=1.8, h=5, $fn=18);

    translate([-3.5, 2.5, 2]) cube([7, 1, 2]);
    translate([-3.5, 0.5, 2]) cube([7, 1, 2]);
    translate([-3.5, -1.5, 2]) cube([7, 1, 2]);
    translate([-3.5, -3.5, 2]) cube([7, 1, 2]);
  }
}

module belt_spanner() {
  difference() {
    translate([0,-3.5,0]) rotate([-90,0,0]) cylinder(r=3.25, h=7, $fn=30);
    translate([-4, -4, -6]) cube([8, 8, 6]);
    translate([0, 0, -1]) cylinder(r=1.65, h=4, $fn=16);
  }
}

module z_belt_mount() {
  translate([-14,0,0]) clamp_bottom();
  translate([-33.5,0,24.5]) rotate([90,0,-90]) belt_clamp1();
  translate([-46.5,0,24.5]) rotate([90,0,90]) belt_clamp2();
}

module print_bed() {
  //translate([  0,  15, 0]) rotate([0,0,180]) clamp_bottom();
  translate([  0,  15, 0]) rotate([0,0,180]) clamp_bottom_with_endstop();
  //translate([-30,  20, 0]) belt_clamp1();
  //translate([-30,  -15, 0]) belt_clamp2();
  //translate([ 15, -15, 0]) belt_spanner();

  //translate([0.5, -24, 0]) cube([10, 10, 1]);
  //translate([10, -19, 0]) cube([2, 1, 1]);
}

//z_belt_mount();
mirror([0,1,0]) print_bed();
