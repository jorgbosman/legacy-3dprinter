Owobi Bot
=========

A compact 3d printer, 40x40x60cm build platform, 1.75mm direct-drive extruder. Main focus is on high quality and silent printing.

The idea is to stick to proven technology. No fancy stuff, no experimental stuff. Just plain solid basic 3d printing.

