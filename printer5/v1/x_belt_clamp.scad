// Y belt clamp
//
// jorg@bosman.tv - 2014-08-23

module left() {
  difference() {
    union() {
      translate([-5,-11,0]) cube([9.8,22,7]);
    }
    translate([-0.8,0,-1]) cylinder(r=2.7, h=9, $fn=17);
    translate([-0.8,0,5]) rotate([0,0,90]) cylinder(r=4.6, h=3, $fn=6);

    for (i = [-5:5]) {
      translate([ 4,i*2+0.5,-1]) cube([1.5,1,9]);
    }
  }
}

module right() {
  difference() {
    union() {
      translate([-4.8,-11,0]) cube([9.8,22,7]);
    }
    translate([-0.8,0,-1]) cylinder(r=2.7, h=9, $fn=17);
    translate([-0.8,0,5]) rotate([0,0,90]) cylinder(r=4.6, h=3, $fn=6);

    for (i = [-5:5]) {
      translate([-5,i*2-0.5,-1]) cube([1,1,9]);
    }
  }
}

translate([-12,0,0]) left();
right();
