// Z Spacer Block, between the Z-axis and the X-axis-frame
//
// jorg@bosman.tv - 2014-08-23

module block() {
  difference() {
    union() {
      hull() {
        translate([-20, -10, 0]) cube([40,20,2]);
        translate([-17, -10, 20-2]) cube([34,20,2]);
      }
    }
    translate([-10,0,-1]) cylinder(r=2.8, h=22, $fn=28);
    translate([+10,0,-1]) cylinder(r=2.8, h=22, $fn=28);
  }
}

block();
