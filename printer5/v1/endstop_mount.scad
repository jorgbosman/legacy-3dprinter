// Endstop mount
//
// jorg@bosman.tv - 2014-08-17

module endstop(z) {
    difference() {
        union() {
            translate([0,0,0]) cube([40,10,3]);
            hull() {
                translate([7.5,10,0]) cylinder(r=7.5, h=3, $fn=75);
                translate([32.5,10,0]) cylinder(r=7.5, h=3, $fn=75);
            }
            translate([13,0,3]) cube([14,33,3]);
            translate([13,23,0]) cube([14,10,3]);

            translate([20,18,3]) cylinder(r=1.2, h=5, $fn=12);
        }
        translate([5,10,-1]) cylinder(r=2.8, h=5, $fn=28);
        translate([35,10,-1]) cylinder(r=2.8, h=5, $fn=28);

        if (z == true) {
          translate([20,10,-1]) cylinder(r=2.8, h=8, $fn=28);
          translate([20,10,4]) cylinder(r=5, h=4, $fn=28);
          translate([-1,6,-1]) cube([14,8,2]);
        }
        translate([20,28,-1]) cylinder(r=1.7, h=8, $fn=17);
    }
}

endstop(true);
//translate([-45,0,0]) endstop(false);
