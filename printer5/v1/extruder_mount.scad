// Extruder mount for Bulldog Lite with e3d hotend
//
// jorg@bosman.tv - 2014-08-30

module mount() {
  difference() {
    union() {
      translate([-26,-41,0]) cube([64,71,4]);
      hull() {
        translate([-20,30,0]) cylinder(r=6,h=4,$fn=60);
        translate([ 32,30,0]) cylinder(r=6,h=4,$fn=60);
      }

      //top hotend mount
      translate([8,17.3,0]) cube([22.3,4,12.5]);
      translate([14,17.3,13.5]) rotate([-90,0,0]) cylinder(r=6,h=4,$fn=50);
      translate([14,17.3,1]) cube([17.3,4,18.5]);
      translate([27.3,9.3,0]) cube([4,12,19.5]);
      
      //bottom hotend mount
      translate([-26,-41,0]) cube([64,3.7,26]);
      translate([-26,-41,0]) cube([3.7,12,26]);
      translate([-26+60.3,-41,0]) cube([3.7,12,26]);
      translate([-26,-41+8.3,0]) cube([10.7,3.7,26]);
      translate([-26+53.3,-41+8.3,0]) cube([10.7,3.7,26]);

    }

    // top holes
    translate([-20,30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([  0,30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([ 20,30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([-20,30, 2.5]) cylinder(r=5, h=2, $fn=50);
    translate([  0,30, 2.5]) cylinder(r=5, h=2, $fn=50);
    translate([ 20,30, 2.5]) cylinder(r=5, h=2, $fn=50);

    // center holes
    translate([-9,-4, 2]) cylinder(r=3.5, h=4, $fn=27);
    translate([ 6,-4,-1]) cylinder(r=7.5, h=6, $fn=50);

    //bottom holes
    //translate([-20,-30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([  0,-30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([ 20,-30,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([  0,-30,2.5]) cylinder(r=5, h=6, $fn=50);
    translate([ 20,-30,2.5]) cylinder(r=5, h=6, $fn=50);

    //extruder mounting holes
    translate([14,17,13.5]) rotate([-90,0,0]) cylinder(r=2.7, h=6, $fn=27);
    translate([-18.5,-42,14.5]) rotate([-90,0,0]) cylinder(r=1.7, h=14, $fn=17);
    translate([ 30.5,-42,14.5]) rotate([-90,0,0]) cylinder(r=1.7, h=14, $fn=17);
    translate([-18.5,-42,14.5]) rotate([-90,0,0]) cylinder(r=3.1, h=3, $fn=6);
    translate([ 30.5,-42,14.5]) rotate([-90,0,0]) cylinder(r=3.1, h=3, $fn=6);

    //hotend hole
    translate([0,-42,13]) rotate([-90,0,0]) cylinder(r=10,h=6,$fn=100);
    translate([-10,-42,13]) cube([20,6,14]);

    //fan mount
    translate([-27,-43,-1]) cylinder(r=2.7, h=28, $fn=27);
    translate([ 27,-43,-1]) cylinder(r=2.7, h=28, $fn=27);

  }
}

module buildplate() {
  difference() {
    union() {
      translate([-70,-60,0]) color("gray") cube([140,120,3.2]);
    }
    translate([-20,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
  }
}

module extruder() {
  difference() {
    union() {
      color("lightgray") {
        translate([-15,-33,0]) cube([42,54,22]);
        translate([-22,-33,0]) cube([56,4,22]);
      }
    }
  }
}

module hotend() {
  difference() {
    union() {
      color("silver") {
        translate([0,0,-46]) cylinder(r1=0.2, r2=2, h=2, $fn=20);
        translate([0,0,-44]) cylinder(r=4, h=3, $fn=6);
        translate([-8,-16,-41]) cube([16,20,12]);
        translate([0,0,-27]) cylinder(r=11, h=26, $fn=65);
        translate([0,0,-1]) cylinder(r=4.8, h=17, $fn=40);
        translate([0,0, 0.5]) cylinder(r=10.5, h=1, $fn=50);
        translate([0,0, 3]) cylinder(r=10.5, h=3, $fn=50);
        translate([0,0, 3]) cylinder(r=6, h=13, $fn=30);
        translate([0,0,13]) cylinder(r=10.5, h=3, $fn=50);
      }
    }
  }
}

module fan(size) {
  difference() {
    union() {
      color("gray") {
        translate([-(size/2),-(size/2),0]) cube([size,size,10]);
      }
    }
    translate([0,0,-1]) cylinder(r=size/2-.5, h=12, $fn=80);
    translate([-(size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([-(size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([ (size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([ (size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
  }
  color("gray") cylinder(r=size/4+.5, h=10, $fn=50);
}

mount();
translate([0,0,-3.2]) buildplate();
//translate([0,-4, 4]) extruder();
//translate([0,-41,15]) rotate([-90,90,0]) hotend();
//translate([0,-57,30]) fan(30);
//translate([-40,-65,23]) rotate([-40,-80,0]) fan(40);
//translate([ 40,-65,23]) rotate([-40, 80,0]) fan(40);

