// Fan mount
//
// jorg@bosman.tv - 2014-08-30

module fanmount() {
  difference() {
    union() {
      //fan mount left
      translate([-38,-60, 0]) cube([23, 19, 40]);
      translate([-18,-72,10]) cube([3,13,30]);

      //fan mount right
      translate([ 19, -60, 0]) cube([19, 19, 40]);
      translate([ 19, -72,10]) cube([3,13,30]);

      //fan mount middle
      translate([-18,-72,28]) cube([40,31,2]);
    }

    translate([-27,-43,-1]) cylinder(r=2.6, h=42, $fn=26);
    translate([ 27,-43,-1]) cylinder(r=2.6, h=42, $fn=26);
    translate([-27,-43,39]) cylinder(r=5, h=2, $fn=50);
    translate([ 27,-43,39]) cylinder(r=5, h=2, $fn=50);

    //fan mount left
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([-20,-20,0]) cube([40,40,20]);
    translate([-40,-65,23]) rotate([-60,-75,0]) translate([0,0,-5.8]) cylinder(r=20, h=12.8, $fn=200);
    translate([-15,-72,9]) rotate([0,0,135]) cube([5,5,32]);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([-16,  16, -15]) cylinder(r=2.2, h=26, $fn=17);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([ 16,  16, -11]) cylinder(r=2.2, h=26, $fn=17);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([-16,  16, -12]) cylinder(r=4.4, h=5, $fn=6);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([ 16,  16, -12]) cylinder(r=4.4, h=5, $fn=6);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([-11.6-8.8,  12, -12]) cube([8.8,4,5]);
    translate([-40,-65,23]) rotate([-40,-80,0]) translate([ 11.6,  12, -12]) cube([8.8,4,5]);

    //fan mount right
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-20,-20,0]) cube([40,40,20]);
    translate([ 40,-65,23]) rotate([-60, 75,0]) translate([0,0,-5.8]) cylinder(r=20, h=13, $fn=200);
    translate([ 26,-72,9]) rotate([0,0,135]) cube([5,5,32]);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-16,  16, -15]) cylinder(r=2.2, h=26, $fn=17);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([ 16,  16, -11]) cylinder(r=2.2, h=26, $fn=17);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-16,  16, -12]) cylinder(r=4.4, h=5, $fn=6);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([ 16,  16, -12]) cylinder(r=4.4, h=5, $fn=6);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-11.6-8.8,  12, -12]) cube([8.8,4,5]);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([ 11.6,  12, -12]) cube([8.8,4,5]);

    //fan mount middle
    translate([-19+15+4,-72+15, 27]) cylinder(r=15, h=4, $fn=150);
    translate([-19+7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([-19+7,-72+27, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+27, 27]) cylinder(r=1.7, h=4, $fn=17);
  }

  // fan mount right
  // fan mount middle
  difference() {
    union() {
      translate([ 19, -72,10]) cube([3,16,30]);
      translate([-18,-72,28]) cube([40, 9, 2]);
    }
    translate([-19+15+4, -72+15+6, 28+7]) rotate([40,0,0]) cylinder(r=18, h=12, $fn=150);
    translate([-15,-72,9]) rotate([0,0,135]) cube([5,5,32]);
    translate([ 26,-72,9]) rotate([0,0,135]) cube([5,5,32]);
    translate([-19+7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-16,  16, -15]) cylinder(r=2.2, h=26, $fn=17);
    translate([ 40,-65,23]) rotate([-40, 80,0]) translate([-16,  16, -12]) cylinder(r=4.4, h=5, $fn=6);
  }
}

module buildplate() {
  difference() {
    union() {
      translate([-70,-60,0]) color("gray") cube([140,120,3.2]);
    }
    translate([-20,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
  }
}

module extruder() {
  difference() {
    union() {
      color("lightgray") {
        translate([-15,-33,0]) cube([42,54,22]);
        translate([-22,-33,0]) cube([56,4,22]);
      }
    }
  }
}

module hotend() {
  difference() {
    union() {
      color("silver") {
        translate([0,0,-46]) cylinder(r1=0.2, r2=2, h=2, $fn=20);
        translate([0,0,-44]) cylinder(r=4, h=3, $fn=6);
        translate([-8,-16,-41]) cube([16,20,12]);
        translate([0,0,-27]) cylinder(r=11, h=26, $fn=65);
        translate([0,0,-1]) cylinder(r=4.8, h=17, $fn=40);
        translate([0,0, 0.5]) cylinder(r=10.5, h=1, $fn=50);
        translate([0,0, 3]) cylinder(r=10.5, h=3, $fn=50);
        translate([0,0, 3]) cylinder(r=6, h=13, $fn=30);
        translate([0,0,13]) cylinder(r=10.5, h=3, $fn=50);
      }
    }
  }
}

module fan(size) {
  difference() {
    union() {
      color("gray") {
        translate([-(size/2),-(size/2),0]) cube([size,size,10]);
      }
    }
    translate([0,0,-1]) cylinder(r=size/2-.5, h=12, $fn=80);
    translate([-(size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([-(size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([ (size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
    translate([ (size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=12, $fn=10);
  }
  color("gray") cylinder(r=size/4+.5, h=10, $fn=50);
}

fanmount();
translate([0,0,-3.2]) buildplate();
//translate([0,-4, 4]) extruder();
//translate([0,-41,15]) rotate([-90,90,0]) hotend();
//translate([0,-57,30]) fan(30);
translate([-40,-65,23]) rotate([-40,-80,0]) fan(40);
//translate([ 40,-65,23]) rotate([-40, 80,0]) fan(40);

