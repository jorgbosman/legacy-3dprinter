// Y axis endstop mount plate
//
// jorg@bosman.tv - 2014-08-23

module endstop() {
  difference() {
    union() {
      translate([-10,-21,0]) cube([5,42,3]);
      hull() {
        translate([-5, -16, 0]) cylinder(r=5, h=3, $fn=50);
        translate([-5,  16, 0]) cylinder(r=5, h=3, $fn=50);
      }
      translate([-1, -16,-2]) cube([1,32,4]);
      translate([ 0, -7, -10]) cube([3,14,20]);

      translate([0, 0, -5]) rotate([0,90,0]) cylinder(r=1.2, h=5, $fn=17);
    }
    hull() {
      translate([-5.2, -16, -1]) cylinder(r=1.7, h=5, $fn=17);
      translate([-4.8, -16, -1]) cylinder(r=1.7, h=5, $fn=17);
    }
    hull() {
      translate([-5.2,  16, -1]) cylinder(r=1.7, h=5, $fn=17);
      translate([-4.8,  16, -1]) cylinder(r=1.7, h=5, $fn=17);
    }

    translate([-21, 0, -1]) cylinder(r=12.5, h=5, $fn=125);

    translate([-1, 0, 5]) rotate([0,90,0]) cylinder(r=1.7, h=5, $fn=17);
    translate([-3, 0, 5]) rotate([0,90,0]) cylinder(r=3, h=3, $fn=17);
  }
}

rotate([0,-180,0]) endstop();
