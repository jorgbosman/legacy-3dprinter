// controller box
//
// jorg@bosman.tv - 2014-08-26

use <feet.scad>;

module bottom_plate() {
  difference() {
    union() {
      translate([0,0,0]) cube([120,145,3]);

      // front side
      translate([20,0,0]) cube([100,3,23]);
      translate([40,0,0]) cube([80,3,40]);

      // right side
      translate([117,0,0]) cube([3,125,40]);

      // left side
      translate([0,20,0]) cube([23,100,18]);
      translate([0,20,0]) cube([23,80,23]);
      translate([20,99,0]) cube([3,26,23]);
      translate([20,0,0]) cube([3,24,23]);

      // back side
      translate([20,122,0]) cube([100,3,23]);

      // board holes
      translate([24.5+3,    4.5+3,     0]) cylinder(r=3, h=6, $fn=17);
      translate([24.5+3,    4.5+110-3, 0]) cylinder(r=3, h=6, $fn=17);
      translate([24.5+91-3, 4.5+3,     0]) cylinder(r=3, h=6, $fn=17);
      translate([24.5+91-3, 4.5+110-3, 0]) cylinder(r=3, h=6, $fn=17);
      translate([20, 0, 0]) cube([4.5+3,10.5,6]);
      translate([20, 0, 0]) cube([10.5,4.5+3,6]);
      translate([24.5+91-3, 0, 0]) cube([4.5+3,10.5,6]);
      translate([24.5+91-6, 0, 0]) cube([10.5,4.5+3,6]);
      translate([20, 4.5+110-6, 0]) cube([4.5+3,10.5,6]);
      translate([20, 4.5+110-3, 0]) cube([10.5,4.5+9,6]);
      translate([24.5+91-3, 4.5+110-6, 0]) cube([4.5+3,10.5,6]);
      translate([24.5+91-6, 4.5+110-3, 0]) cube([10.5,4.5+9,6]);
    }

    // right side
    for (i = [0:10]) {
      hull() {
        translate([116,10+i*10,15]) rotate([0,90,0]) cylinder(r=2.5, h=5, $fn=25);
        translate([116,10+i*10,30]) rotate([0,90,0]) cylinder(r=2.5, h=5, $fn=25);
      }
    }

    // front side
    for (i = [0:6]) {
      hull() {
        translate([50+i*10, -1, 15]) rotate([-90,0,0]) cylinder(r=2.5, h=5, $fn=25);
        translate([50+i*10,-1, 30]) rotate([-90,0,0]) cylinder(r=2.5, h=5, $fn=25);
      }
    }

    // left side
    translate([-1,-1,-1]) cube([21,21,5]);
    translate([10,69,-1]) cylinder(r=2.7, h=22, $fn=27);
    translate([10,27,-1]) cylinder(r=2.7, h=22, $fn=27);
    translate([-1,74.5, 6]) cube([25,21,13]);
    translate([24.5-3,4.5+34,6]) cube([8,9,10]);
    translate([24.5-5,4.5+52,6]) cube([21,16,4]);

    // back side
    translate([30,135,-1]) cylinder(r=2.7, h=5, $fn=27);
    translate([110,135,-1]) cylinder(r=2.7, h=5, $fn=27);
    hull() {
      translate([66,120-3,-1]) cylinder(r=3,h=5,$fn=25);
      translate([104,120-3,-1]) cylinder(r=3,h=5,$fn=25);
    }
    translate([30,121,13]) rotate([-90,0,0]) cylinder(r=2.7, h=5, $fn=27);
    translate([110,121,13]) rotate([-90,0,0]) cylinder(r=2.7, h=5, $fn=27);

    // board holes
    translate([24.5+2.5,    4.5+2.5,     -1]) cylinder(r=1.7, h=8, $fn=17);
    translate([24.5+2.5,    4.5+105+2.5, -1]) cylinder(r=1.7, h=8, $fn=17);
    translate([24.5+86+2.5, 4.5+2.5,     -1]) cylinder(r=1.7, h=8, $fn=17);
    translate([24.5+86+2.5, 4.5+105+2.5, -1]) cylinder(r=1.7, h=8, $fn=17);

    // foot cut-out
    translate([-1,32,-1]) cube([21,32,25]);
    translate([-1,19,19]) cube([21,58,5]);
  }
  difference() {
    translate([0,74.5, 13.5]) rotate([45,0,0]) cube([23,8,5]);
    translate([-1,19,19]) cube([21,58,5]);
  }
}

module megatronics_board() {
  difference() {
    union() {
      color("red") cube([91, 110, 1]);
      translate([-8,78,1]) color("silver") cube([16,12,11]);
      translate([-2,35,1]) color("black") cube([6,7,8]);
      translate([-4,53,1]) color("silver") cube([19,14,2]);
    }
    translate([2.5,2.5,-1]) cylinder(r=1.7, h=3, $fn=17);
    translate([2.5,105+2.5,-1]) cylinder(r=1.7, h=3, $fn=17);
    translate([86+2.5,3,-1]) cylinder(r=1.7, h=3, $fn=17);
    translate([86+2.5,105+2.5,-1]) cylinder(r=1.7, h=3, $fn=17);
  }
}

module frame() {
  difference() {
    union() {
      #translate([-40,20,23]) cube([60,165,20]);
      #translate([-140,125,3]) cube([260,60,20]);
    }
  }
}

bottom_plate();
//translate([24.5,4.5,6]) megatronics_board();
//frame();
//translate([-10,48,23]) rotate([180,0,0]) foot(true);
