// Fan mount
//
// jorg@bosman.tv - 2014-08-30

module fanmount() {
  difference() {
    union() {
      //fan mount left
      translate([-38,-61, 0]) cube([23, 20, 40]);
      hull() {
        translate([-42,-65,23]) rotate([-40,-80,0]) translate([0,0,-2]) cylinder(r=21, h=2, $fn=200);
        translate([-42,-65,23]) rotate([-36,-80,0]) translate([0,0,-26]) cube([32,6,5], center=true);
        // printing support
        translate([-42,-65,1]) rotate([-40,-90,0]) translate([0,0,2]) cube([2,40,2], center=true);
        translate([-45,-65.5,1]) rotate([-36,-90,0]) translate([0,0,-26]) cube([2,6,5], center=true);
      }

      //fan mount right
      translate([19, -60, 0]) cube([19, 19, 40]);
      hull() {
        translate([ 44,-65,23]) rotate([-40, 80,0]) translate([0,0,-2]) cylinder(r=21, h=2, $fn=200);
        translate([ 44,-65,23]) rotate([-36, 80,0]) translate([0,0,-26]) cube([32,6,5], center=true);
        // printing support
        translate([ 44,-65,1]) rotate([-40, 90,0]) translate([0,0,2]) cube([2,40,2], center=true);
        translate([ 47,-65.5,1]) rotate([-36, 90,0]) translate([0,0,-26]) cube([2,6,5], center=true);
      }

      //fan mount middle
      translate([-23,-73,28]) cube([50,32,2]);
    }

    translate([-27,-43,-1]) cylinder(r=2.6, h=42, $fn=26);
    translate([ 27,-43,-1]) cylinder(r=2.6, h=42, $fn=26);
    translate([-27,-43,39]) cylinder(r=5, h=2, $fn=50);
    translate([ 27,-43,39]) cylinder(r=5, h=2, $fn=50);

    //fan mount left
    // cutout to mount the fan into
    translate([-42,-65,23]) rotate([-40,-80,0]) translate([-20,-20,0]) cube([40,40,20]);
    // big fan opening
    hull() {
      translate([-42,-65,23]) rotate([-40,-80,0]) translate([0,0,0]) cylinder(r=20, h=2, $fn=200);
      translate([-42,-65,23]) rotate([-36,-80,0]) translate([0,0,-26]) cube([30,5,5], center=true);
    }
    translate([-42,-65,23]) rotate([-36,-80,0]) translate([0,0,-26]) cube([30,5,11], center=true);
    // holes for fan-screws
    translate([-42,-65,23]) rotate([-40,-80,0]) translate([-16,  16, -15]) cylinder(r=2.2, h=26, $fn=17);
    translate([-42,-65,23]) rotate([-40,-80,0]) translate([-16,  16, -22]) cylinder(r=4.4, h=15, $fn=6);
    translate([-42,-65,23]) rotate([-40,-80,0]) translate([-11.6-8.8-4,  12, -18]) cube([8.4,8,11]);
 
    //fan mount right
    // cutout to mount the fan into
    translate([ 44,-65,23]) rotate([-40, 80,0]) translate([-20,-20,0]) cube([40,40,20]);
    // big fan opening
    hull() {
      translate([ 44,-65,23]) rotate([-40, 80,0]) translate([0,0,0]) cylinder(r=20, h=2, $fn=200);
      translate([ 44,-65,23]) rotate([-36, 80,0]) translate([0,0,-26]) cube([30,5,5], center=true);
    }
    translate([ 44,-65,23]) rotate([-36, 80,0]) translate([0,0,-26]) cube([30,5,11], center=true);
 
    // holes for fan-screws
    translate([ 44,-65,23]) rotate([-40, 80,0]) translate([ 16,  16, -11]) cylinder(r=2.2, h=26, $fn=17);
    translate([ 44,-65,23]) rotate([-40, 80,0]) translate([ 16,  16, -22]) cylinder(r=4.4, h=15, $fn=6);
    translate([ 44,-65,23]) rotate([-40, 80,0]) translate([ 11.6+4,  12, -18]) cube([8.4,8,11]);

    //fan mount middle
    translate([-19+7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([-19+7,-72+27, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+27, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([-19+15+4,-72+15, 27]) cylinder(r=14.9, h=4, $fn=100);
  }

  // fan mount right
  // fan mount middle
  difference() {
    union() {
      translate([-18,-72,28]) cube([36, 9, 2]);
      // fan bolt
      translate([-42,-65,23]) rotate([-40,-80,0]) translate([ 16,  16, -6]) cylinder(r=2, h=8, $fn=17);
      translate([ 44,-65,23]) rotate([-40, 80,0]) translate([-16,  16, -6]) cylinder(r=2.2, h=8, $fn=17);
    }
    translate([-19+15+4, -72+15+6, 28+7]) rotate([40,0,0]) cylinder(r=18, h=12, $fn=150);
    translate([-19+7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
    translate([ 19-7,-72+3, 27]) cylinder(r=1.7, h=4, $fn=17);
  }
}

module buildplate() {
  difference() {
    union() {
      translate([-70,-60,0]) color("gray") cube([140,120,3.2]);
    }
    translate([-20,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0, 0,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20, 0,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([  0,-30,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 20,-30,-1]) cylinder(r=2.6, h=5, $fn=26);

    translate([-27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
    translate([ 27,-43,-1]) cylinder(r=2.6, h=5, $fn=26);
  }
}

module extruder() {
  difference() {
    union() {
      color("lightgray") {
        translate([-15,-33,0]) cube([42,54,22]);
        translate([-22,-33,0]) cube([56,4,22]);
      }
    }
  }
}

module hotend() {
  difference() {
    union() {
      color("silver") {
        translate([0,0,-46]) cylinder(r1=0.2, r2=2, h=2, $fn=20);
        translate([0,0,-44]) cylinder(r=4, h=3, $fn=6);
        translate([-8,-16,-41]) cube([16,20,12]);
        translate([0,0,-27]) cylinder(r=11, h=26, $fn=65);
        translate([0,0,-1]) cylinder(r=4.8, h=17, $fn=40);
        translate([0,0, 0.5]) cylinder(r=10.5, h=1, $fn=50);
        translate([0,0, 3]) cylinder(r=10.5, h=3, $fn=50);
        translate([0,0, 3]) cylinder(r=6, h=13, $fn=30);
        translate([0,0,13]) cylinder(r=10.5, h=3, $fn=50);
      }
    }
  }
}

module fan(size,height) {
  difference() {
    union() {
      color("gray") {
        translate([-(size/2),-(size/2),0]) cube([size,size,height]);
      }
    }
    translate([0,0,-1]) cylinder(r=size/2-.5, h=height+2, $fn=80);
    translate([-(size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=height+2, $fn=10);
    translate([-(size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=height+2, $fn=10);
    translate([ (size/2-size/10),-(size/2-size/10),-1]) cylinder(r=size/20, h=height+2, $fn=10);
    translate([ (size/2-size/10), (size/2-size/10),-1]) cylinder(r=size/20, h=height+2, $fn=10);
  }
  color("gray") cylinder(r=size/4+.5, h=height, $fn=50);
}

fanmount();
//translate([0,0,-3.2]) buildplate();
//translate([0,-4, 4]) extruder();
//translate([0,-41,15]) rotate([-90,90,0]) hotend();
//translate([0,-57,30]) fan(30,10);
//translate([-42,-65,23]) rotate([-40,-80,0]) fan(40,20);
//translate([ 44,-65,23]) rotate([-40, 80,0]) fan(40,20);

