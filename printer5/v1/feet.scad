// Foot
//
// jorg@bosman.tv - 2014-08-19

module foot(big) {
  height = (big == true) ? 50-24 : 30-24; // rubber foot is 24mm

  difference() {
    union() {
      translate([-30,-15,0]) cube([60,30,height+2]);

      hull() {
        translate([-26,-24,0]) cylinder(r=4, h=4, $fn=40);
        translate([-14,-24,0]) cylinder(r=4, h=4, $fn=40);
      }
      translate([-30,-24,0]) cube([20,20,4]);

      hull() {
        translate([ 26,-24,0]) cylinder(r=4, h=4, $fn=40);
        translate([ 14,-24,0]) cylinder(r=4, h=4, $fn=40);
      }
      translate([ 10,-24,0]) cube([20,20,4]);

      if (big == true) {
        translate([-30, 4,0]) cube([20,24,4]);
        translate([ 10, 4,0]) cube([20,24,4]);
      }
    }

    translate([-20,-21,-1]) cylinder(r=2.7, h=6, $fn=27);
    translate([ 20,-21,-1]) cylinder(r=2.7, h=6, $fn=27);
    if (big == true) {
      translate([-20, 21,-1]) cylinder(r=2.7, h=6, $fn=27);
      translate([ 20, 21,-1]) cylinder(r=2.7, h=6, $fn=27);
    } else {
      translate([  0, 7.5,-1]) cylinder(r=2.7, h=10, $fn=27);
      translate([  0, 7.5,4]) cylinder(r=5, h=5, $fn=27);
    }

    difference() {
      translate([-15,0,height]) cylinder(r=15.1, h=3, $fn=155);
      translate([-15,0,0]) cylinder(r=5, h=height+2, $fn=50);
    }
    difference() {
      translate([ 15,0,height]) cylinder(r=15.1, h=3, $fn=155);
      translate([ 15,0,0]) cylinder(r=5, h=height+2, $fn=50);
    }

    translate([-15,0,-1]) cylinder(r=2.7, h=height+4, $fn=27);
    translate([ 15,0,-1]) cylinder(r=2.7, h=height+4, $fn=27);

    translate([-15,0,-1]) cylinder(r=4.9, h=height-1, $fn=6);
    translate([ 15,0,-1]) cylinder(r=4.9, h=height-1, $fn=6);

    // edges
    translate([-29,-30,0]) rotate([0,-135,0]) cube([5,60,5]);
    translate([ 29,-30,0]) rotate([0,45,0]) cube([5,60,5]);
    translate([-29,15,4]) rotate([0,0,135]) cube([5,5,height-1]);
    translate([ 29,15,4]) rotate([0,0,-45]) cube([5,5,height-1]);
    translate([-29,-15,4]) rotate([0,0,135]) cube([5,5,height-1]);
    translate([ 29,-15,4]) rotate([0,0,-45]) cube([5,5,height-1]);

  }
}

translate([0,-30,0]) foot(false);
translate([0, 30,0]) foot(true);
