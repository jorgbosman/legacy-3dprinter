// Foot, front side and back side
// Underneath a 80mm beam
// Together with 4 Sorbothane 40mm feet, it should lift the beam 51mm off of the ground
// This gives 3mm room underneath the stepper motors
//
// jorg@bosman.tv - 2014-08-19

use <include/foot.scad>;

foot(beam_height=51, beam_width=80, sorbothane_radius=20);
