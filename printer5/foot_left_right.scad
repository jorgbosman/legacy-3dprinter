// Foot, front side and back side
// Underneath a 60mm beam
// Together with 4 Sorbothane 30mm feet, it should lift the beam 31mm off of the ground
// This gives 3mm room underneath the stepper motors
//
// jorg@bosman.tv - 2014-08-19

use <include/foot.scad>;

foot(beam_height=31, beam_width=60, sorbothane_radius=15);
