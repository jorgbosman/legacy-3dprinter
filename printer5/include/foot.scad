// Foot, front side and back side
// Underneath a 80mm beam
// Together with 4 Sorbothane 40mm feet, it should lift the beam 51mm off of the ground
// This gives 3mm room underneath the stepper motors
//
// jorg@bosman.tv - 2014-08-19

module foot(beam_height=51, beam_width=80, sorbothane_radius=20) {
  // beam_height: between floor and beam (31mm or 51mm)
  // beam_width: beams used are either 80x20mm or 60x20mm
  // sorbothane_radius: radius of the sorbothane round foot (15mm or 20mm)
  height = beam_height - sorbothane_radius;
    
  difference() {
    union() {
      // main block
      translate([-(beam_width/2),-(beam_width/2),0]) cube([beam_width,beam_width,height+2]);

      // 2 pieces sticking out the side
      hull() {
        translate([-(beam_width/2)+4, -(beam_width/2)-14,0]) cylinder(r=4, h=4, $fn=40);
        translate([-(beam_width/2)+16,-(beam_width/2)-14,0]) cylinder(r=4, h=4, $fn=40);
      }
      translate([-(beam_width/2), -(beam_width/2)-14,0]) cube([20,20,4]);

      hull() {
        translate([(beam_width/2)-4, -(beam_width/2)-14,0]) cylinder(r=4, h=4, $fn=40);
        translate([(beam_width/2)-16, -(beam_width/2)-14,0]) cylinder(r=4, h=4, $fn=40);
      }
      translate([(beam_width/2)-20, -(beam_width/2)-14,0]) cube([20,20,4]);
    }

    translate([-(beam_width/2)+10, -(beam_width/2)-11,-1]) cylinder(r=2.8, h=6, $fn=27);
    translate([(beam_width/2)-10, -(beam_width/2)-11,-1]) cylinder(r=2.8, h=6, $fn=27);

    // mounting holes inside the block
    translate([-(beam_width/2)+10, (beam_width/2)-9,-1]) cylinder(r=2.8, h=10, $fn=27);
    translate([-(beam_width/2)+10, (beam_width/2)-9,4]) cylinder(r=5, h=height-1, $fn=27);

    translate([ (beam_width/2)-10, (beam_width/2)-9,-1]) cylinder(r=2.8, h=10, $fn=27);
    translate([ (beam_width/2)-10, (beam_width/2)-9,4]) cylinder(r=5, h=height-1, $fn=27);

    // cutouts for the sorbothane feet
    translate([-(beam_width/2)+sorbothane_radius,(beam_width/2)-sorbothane_radius,height]) cylinder(r=sorbothane_radius+0.1, h=3, $fn=155);
    translate([ (beam_width/2)-sorbothane_radius,(beam_width/2)-sorbothane_radius,height]) cylinder(r=sorbothane_radius+0.1, h=3, $fn=155);
    translate([-(beam_width/2)+sorbothane_radius,-(beam_width/2)+sorbothane_radius,height]) cylinder(r=sorbothane_radius+0.1, h=3, $fn=155);
    translate([ (beam_width/2)-sorbothane_radius,-(beam_width/2)+sorbothane_radius,height]) cylinder(r=sorbothane_radius+0.1, h=3, $fn=155);
    
    // bottom edges
    translate([-(beam_width/2)+0.5,-(beam_width/2)-20,0]) rotate([0,-112.5,0]) cube([5,beam_width+21,5]);
    translate([ (beam_width/2)-0.5,-(beam_width/2)-20,0]) rotate([0,22.5,0]) cube([5,beam_width+21,5]);
    translate([(beam_width/2)+1,(beam_width/2)-0.5,0]) rotate([0,22.5,90]) cube([5,beam_width+2,5]);

    // side edges  
    translate([-(beam_width/2)+1,beam_width/2,-1]) rotate([0,0,135]) cube([5,5,height+4]);
    translate([ (beam_width/2)-1,beam_width/2,-1]) rotate([0,0,-45]) cube([5,5,height+4]);
    translate([-(beam_width/2)+1,-(beam_width/2),4]) rotate([0,0,135]) cube([5,5,height-1]);
    translate([ (beam_width/2)-1,-(beam_width/2),4]) rotate([0,0,-45]) cube([5,5,height-1]);

  }
}

foot(beam_height=31, beam_width=60, sorbothane_radius=15);
//foot(beam_height=51, beam_width=80, sorbothane_radius=20);
